Opis projektu
===============================================================================

### Nazwa projektu: 

Multichat v2

### Opis projektu: 

Główny serwer jest jedynie rejestrem, w którym
rejestrują się klienci (wpis rejestru zawiera ip i port,
nazwa użytkownika).
Aplikacje klienckie łączą się z serwerem, w celu
pozyskania informacji na temat potencjalnych
rozmówców. Po wyborze przez użytkownika osoby do
rozmowy, zestawiane jest połączenie z aplikacją tego
drugiego (która teraz będzie pełnić rolę serwera) –
komunikacja pomiędzy klientami jest realizowana w
oparciu o model p2p (peer to peer).

Wymagania:
===============================================================================

Na ocenę 3 można wykonać projekt pt. multichat TCP + przesyłanie
plików. Aplikacja ma być konsolowa.

Wymagana funkcjonalność:

1. Użytkownik aplikacji klienckiej będzie mógł podać swój login.  Login powinien być
unikalny, więc serwer będzie musiał reagować, na powielenie się loginu u różnych
klientów. 
2. Po określeniu unikalnego loginu, serwer roześle do podłączonych klientów informacje o
pojawieniu się nowego uczestnika czatu, natomiast ten nowo-podłączony otrzyma
pełną listę loginów użytkowników, którzy byli już podłączeni,
3. Wiadomości mają być adresowane poprzez poprzedzenie ich loginem uczestnika czatu,
do którego ma być przesłana wiadomość.  Login nadawcy jest automatycznie
doklejany do wiadomości.  Przykładowy format wiadomości - od:do:wiadomość. 
4. Przy rozłączeniu się danego uczestnika czatu, do pozostałych powinna być wysłana
informacja, że uczestnik o danym loginie opuścił czat. 
5. Uczestnik czatu ma mieć także możliwość wysłania pliku do katalogu na serwerze a
także jego pobrania. 

UWAGA:

1. Dla każdego projektu, należy opracować protokół komunikacyjny.
Można wykorzystać komendy protokołu HTTP lub FTP.
2. Do projektu powinna być załączona dokumentacja wykonana w
javadoc.
6. Aplikacje klienckie powinny posiadać interfejs graficzny wykonany z
wykorzystaniem biblioteki Swing lub JavaFX.
7. Na ocenę 5 mogą liczyć osoby, które zrealizują projekt z
wykorzystaniem klas URLConnection, HttpURLConnection, SSLSocket
lub DatagramSocket (np., gniazda TCP można wykorzystać tylko po to,
aby zestawić sesję między użytkownikami, a potem komunikować się
na gniazdach UDP). Komunikacja na gniazdach UDP ma być bezstratna,
co wiąże się z zaimplementowaniem warstwy kontroli związanej z
przepływem danych.

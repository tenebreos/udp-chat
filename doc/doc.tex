\documentclass[a4paper]{article}

\usepackage{polyglossia}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{multirow}
\usepackage{booktabs}
\usepackage{float}
\usepackage{IEEEtrantools}
\usepackage{hyperref}
\usepackage{siunitx}
\usepackage{minted}
\usepackage{tikz}
\usepackage{pdflscape}

\setdefaultlanguage{polish}
\usetikzlibrary{arrows,automata,positioning}

\textwidth=16cm
\textheight=25cm
\topmargin=-2cm
\oddsidemargin=0cm

\title{UDP Chat}
\author{Maciej Marczak}
\date{}

\begin{document}
\maketitle

\section{Opis projektu}
Projekt realizuje warstwę transportową niezależną od aplikacji oraz
przykładową aplikację na niej zaimplementowaną. Warstwa transportowa to
protokół połączeniowy typu Go-Back-N nazywany Trivial Transfer Protocol (TTP), 
a aplikacja to chat na architekturze hybrydowej, nazywany UDP-Chat'em.

\section{Opis warstwy transportowej}
\subsection{Opis usług}
Protokół transportowy realizuje abstrakcję bezstratnego kanału dla wiadomości. 
Poprzez dodatek w warstwie aplikacji, może również realizować abstrakcję 
bezstratnego strumienia bajtów. Protokół jest zorientowany na połączenia i 
gwarantuje:
\begin{enumerate}
	\item Dostarczenie wiadomości z zachowaniem kolejności
	\item Bezstratny transfer wiadomości
\end{enumerate}

\subsection{Opis realizacji}
Warstwę transportową realizuje 5 automatów skończonych. Są to pary
nadajnik/odbiornik dla klienta i serwera oraz gniazdo nasłuchujące dla serwera.
Diagramy automatów załączone są w dalszej części tego dokumentu.

Abstrakcja połączenia udostępniana przez protokół zawiera dwa kanały
komunikacyjne: od nadajnika klienta do odbiornika serwera i od nadajnika serwera
do odbiornika klienta. W każdym ze strumieni przesyłane są segmenty zawierające
dane i potwierdzenia. Dane płyną od nadajnika do odbiornika, a potwierdzenia
odwrotnie. W celu umożliwienia wielu połączeń na raz, protokół definiuje
metody nawiązywania połączeń (handshaking) oraz ich zakańczania (teardown).
Segmenty kontrolne używane w tych metodach traktowane są jako dane i wysyłane
przez nadajniki.

Nawiązanie połączenia zaczyna się od otworzenia gniazda nasłuchującego przez
serwer. Po odebraniu pakietu \verb_SYN_ wysłanego przez klienta, serwer otwiera
nowe gniazdo, na którym będzie przebiegać połączenie i odsyła do klienta segment
\verb_SYNACK_ z numerem portu nowo otwartego gniazda. Klient potwierdza
\verb_SYNACK_ wysłaniem potwierdzenia na odebrany numer portu. 
Proces nawiązania ilustruje Fig.~\ref{fig:handshaking}.
\begin{figure}
	\centering
	\includegraphics[height=16em]{handshaking.pdf}
	\caption{Proces nawiązywania połączenia}
	\label{fig:handshaking}
\end{figure}

Zakończenie połączenia rozpoczyna się gdy jedna ze stron wyśle
segment \verb_FIN_. Druga strona odpowiada potwierdzeniem i czeka aż jej warstwa
aplikacji zdecyduje się zamknąć połączenie. Wysyła wtedy segment \verb_FIN_,
który jest potwierdzany po odbiorze. Strona która odebrała ostatni segment
\verb_FIN_ wyłącza się po timeoucie. Proces zakończenia ilustruje
Fig.~\ref{fig:teardown}.
\begin{figure}
	\centering
	\includegraphics[height=16em]{teardown.pdf}
	\caption{Proces zakończenia połączenia}
	\label{fig:teardown}
\end{figure}

Aby strony kanału komunikacyjnego mogły rozróżniać pomiędzy retransmisjami i
nowymi wiadomościami protokół używa numerów sekwencji. Segmenty wysyłane przez
protokół numerowane są liczbami naturalnymi. Z każdym wysłanym
segmentem zawierającym dane numer sekwencji zwiększa się o 1. Dostarczenie 
segmentu sygnalizowane jest wysłaniem potwierdzenia zawierającego numer
sekwencji ostatniego poprawnie odebranego segmentu, nazywanego numerem
potwierdzenia.

Protokół nie implementuje metody wykrywania błędów transmisji. Usługa ta
realizowana jest przez UDP, na którym bazuje. Straty pakietów wykrywane są
poprzez implementację timeoutów. Przy wysłaniu segmentu danych włączany jest
timer, który jest wyłączany przy odbiorze segmentu potwierdzenia. W przypadku
kiedy dobiegnie on do zera, wszystkie niepotwierdzone fragmenty są ponownie
wysyłane. Klasyfikuje to protokół jako Go-Back-N. Ponieważ retransmisja może
zapełnić łącze, stosuje się ograniczenie ilości wysłanych, niepotwierdzonych
pakietów. Wartość ta to tzw. rozmiar okna. Implementacja w projekcie określa je
jako 10 segmentów. Czas na jaki ustawiany jest timer powinien być 
określany na podstawie czasu podróży segmentów, jednak w implementacji jest
ustawiony na \SI{1}{\second}.

\subsection{Diagramy FSM}
Specyfikacja automatów realizujących warstwę protokołu przedstawiona jest w
formie rozszerzonych diagramów automatów skończonych (extended finite state
machine diagram, EFSM). Zaznaczają one najważniejsze stany automatów, opisując
poboczne zmienne w formie pseudokodu. Pozwala to na zwięzłe zdefiniowanie
zachowania systemu, którego diagram FSM byłby inaczej niewygodnie duży.

Rozszerzony diagram FSM składa się ze stanów i przejść. Przejściu towarzyszy
opis składający się z przyczyny i akcji. Przyczyny powodowane są przez środowisko,
a akcje podejmowane przez automat. Przyczyna zapisywana jest pierwsza i jest
odgradzana od akcji za pomocą poziomej linii. Znak '$\bigwedge$' zamiast przyczyny
oznacza, że akcja dzieje się bez przyczyny. Ten sam znak użyty zamiast akcji
oznacza, że po wystąpieniu przyczyny automat nie podejmuje żadnej akcji
(oprócz zmiany stanu).

Diagramy przedstawiono na Fig.~\ref{fig:ClientSender}, Fig.~\ref{fig:ClientReceiver},
Fig.~\ref{fig:ServerSender}, Fig.~\ref{fig:ServerReceiver} oraz Fig.~\ref{fig:ServerSocket}.
Diagramy używają prymitywów, opisy najważniejszych z których następują:
\begin{itemize}
	\item \mintinline{c}{rdt_send(data)} - "reliable data transport send",
		procedura wywoływana przez aplikację w celu przekazania danych do
		wysyłki warstwie transportowej.
	\item \mintinline{c}{udt_send(sndpkt)} - "unreliable data transport
		send", procedura wywoływana przez bezstratną warstwę transportową 
		(TTP) w celu wysłania segmentu przez stratną warstwę
		transportową (UDP).
	\item \mintinline{c}{rdt_rcv(rcvpkt)} - "reliable data transport
		receive", procedura wywoływana przez środowisko w momencie
		otrzymania pakietu.
	\item \mintinline{c}{deliver_data(data)} - procedura wywoływana przez
		warstwę transportową w celu przekazania otrzymanych danych
		warstwie aplikacji.
	\item \mintinline{c}{seqof(pkt)} - zwraca numer sekwencji segmentu.
	\item \mintinline{c}{ackof(pkt)} - zwraca numer potwierdzenia segmentu.
	\item \mintinline{c}{src_addr(pkt)} - zwraca adres nadawcy segmentu.
	\item \mintinline{c}{src_port(pkt)} - zwraca port nadawcy segmentu.
	\item \mintinline{c}{open_socket(dest_addr, dest_port, initseqnum, initexpectedseqnum)} 
		- otwiera nowe gniazdo TTP, inicjując nadajnik numerem
		\mintinline{c}{initseqnum}, a odbiornik
		\mintinline{c}{initexpectedseqnum}.
	\item \mintinline{c}{local_port(socket)} - zwraca numer portu do którego
		przypisane jest gniazdo.
	\item \mintinline{c}{start_timer()} i \mintinline{c}{stop_timer()} -
		włącza i wyłącza timer. W przypadku odliczenia do zera
		generowane jest przerwanie - \verb_timeout_, lub
		\verb|teardown_timer_timeout| w przypadku timera używanego w
		procesie zakończenia połączenia.
	\item \mintinline{c}{make_pkt(seqnum, SYN)},
		\mintinline{c}{make_pkt(seqnum, FIN)} i
		\mintinline{c}{make_pkt(seqnum, data)} - tworzą i zwracają
		segment danych z numerem sekwencji \verb_seqnum_. Segmenty
		\verb_SYN_ i \verb_FIN_ wysyłane są przez nadajnik i traktowane
		jak dane.
	\item \mintinline{c}{make_pkt(acknum, ACK)} - tworzy i zwraca segment
		potwierdzenia z numerem potwierdzenia \verb_acknum_.
	\item \mintinline{c}{accept()} - procedura wywoływana przez warstwę
		aplikacji w serwerze w celu przyjęcia połączenia.
\end{itemize}

\begin{landscape}
\setminted{escapeinside=||}
\begin{figure}
	\centering
	\begin{tikzpicture}[shorten >=1pt,node distance=10cm,auto,scale=0.65, every node/.style={transform shape}]

		\tikzstyle{every state}=[fill={rgb:black,1;white,10},text width=1.5cm,text centered]

		\node[state,initial,initial text={
			\begin{tabular}{l}
				\multicolumn{1}{c}{ $\bigwedge$ }\\
			\midrule
				\mintinline{c}{ curr_serviced_port = 0 }\\
				\mintinline{c}{ curr_serviced_addr = 0 }\\
				\mintinline{c}{ synack; }\\
				\mintinline{c}{ conn_sock; }\\
			\end{tabular}
		}]			(wait)						{ Wait };
		\node[state]		(wait_for_syn)		[right of=wait]		{ Wait for \verb_SYN_ };
		\node[state]		(wait_for_ack)		[below of=wait]		{ Wait for \verb_ACK_ on a new socket};

		\path[->]
		(wait) edge [bend left] node {
			\begin{tabular}{c}
				\mintinline{c}{ accept() }\\
			\midrule
				$\bigwedge$
			\end{tabular}
		} (wait_for_syn)
		(wait_for_syn) edge [loop right] node {
			\begin{tabular}{c}
				\mintinline{c}{ rdt_rcv(rcvpkt) && !isSYN(rcvpkt) }\\
			\midrule
				$\bigwedge$
			\end{tabular}
		} ()
		edge [] node {
			\begin{tabular}{l}
				\mintinline{c}{ rdt_rcv(rcvpkt) && isSYN(rcvpkt) }\\
			\midrule
				\mintinline{c}{ dest_addr = src_addr(rcvpkt) }\\
				\mintinline{c}{ dest_port = src_port(rcvpkt) }\\
				\mintinline{c}{ curr_serviced_port = dest_port }\\
				\mintinline{c}{ curr_serviced_addr = dest_addr }\\
				\mintinline{c}{ initseqnum = rand() }\\
				\mintinline{c}{ expectedseqnum = seqof(rcvpkt) }\\
				\mintinline{c}{ conn_sock = open_socket(dest_addr, dest_port, initseqnum + 1, expectedseqnum + 1) }\\
				\mintinline{c}{ data = local_port(conn_sock) }\\
				\mintinline[escapeinside=]{c}{ synack = make_pkt(initseqnum, expectedseqnum, SYN | ACK, data) }\\
				\mintinline{c}{ udt_send(synack) }\\
			\end{tabular}
		} (wait_for_ack)
		(wait_for_ack) edge [loop below] node {
			\begin{tabular}{l}
				\mintinline{c}{ rdt_rcv(rcvpkt) && (!isSYN(rcvpkt) }\\
				\mintinline{c}{ || curr_serviced_port != src_port(rcvpkt) }\\
				\mintinline{c}{ || curr_serviced_addr != src_addr(rcvpkt)) }\\
			\midrule
				\multicolumn{1}{c}{ $\bigwedge$ }\\
			\end{tabular}
		} ()
		edge [loop left] node {
			\begin{tabular}{l}
				\mintinline{c}{ rdt_rcv(rcvpkt) && isSYN(rcvpkt) }\\
				\mintinline{c}{ && curr_serviced_port == src_port(rcvpkt) }\\
				\mintinline{c}{ && curr_serviced_addr == src_addr(rcvpkt) }\\
			\midrule
				\mintinline{c}{ udt_send(synack) }
			\end{tabular}
		} ()
		(wait_for_ack) edge [bend left] node {
			\begin{tabular}{l}
				\mintinline{c}{ notify_handshake() }\\
			\midrule
				\mintinline{c}{ return conn_sock }\\
			\end{tabular}
		} (wait)
		;
	\end{tikzpicture}
	\caption{Diagram EFSM gniazda nasłuchującego}
	\label{fig:ServerSocket}
\end{figure}
\begin{figure}
	\centering
	\begin{tikzpicture}[shorten >=1pt,node distance=10cm,scale=0.65, every node/.style={transform shape},auto]

		\tikzstyle{every state}=[fill={rgb:black,1;white,10},text width=1.5cm,text centered]

		\node[state,initial left,initial text={ 
			\begin{tabular}{l}
				\multicolumn{1}{c}{ $\bigwedge$ }\\
			\midrule
				\mintinline{c}{ n1 = rand() }\\
				\mintinline{c}{ nextseqnum = n1 }\\
				\mintinline{c}{ base = n1 }\\
				\mintinline{c}{ unackedQ = {} }\\
				\mintinline{c}{ tosendQ = {} }\\
				\mintinline{c}{ dest_port = server_socket_port }\\
				\mintinline{c}{ send_FIN = false }\\
			\end{tabular}
		}]			(unhandshaken)						{ Not initialised };
		\node[state]		(wait_for_synack)	[right of=unhandshaken]		{ Wait for \verb_SYNACK_ };
		\node[state]		(wait)			[right of=wait_for_synack]	{ Wait };
		\node[state]		(wait_for_ack)		[below of=wait]			{ Wait for \verb_ACK_ };
		\node[state]		(wait_for_fin)		[below of=wait_for_synack]	{ Wait for \verb_FIN_ };
		\node[state]		(shutdown)		[below of=unhandshaken]		{ Shutdown };

		\path[->]
		(unhandshaken) edge [] node { 
			\begin{tabular}{l}
				\mintinline{c}{ hanshake() }\\
			\midrule
				\mintinline{c}{ sndpkt = make_pkt(nextseqnum, SYN) }\\
				\mintinline{c}{ nextseqnum++ }\\
				\mintinline{c}{ timer_start }\\
				\mintinline{c}{ udt_send(sndpkt) }\\
				\mintinline{c}{ enqueue(unackedQ, sendpkt) }\\
			\end{tabular}
		} (wait_for_synack)

		(wait_for_synack) edge [loop above] node {
			\begin{tabular}{l}
				\mintinline{c}{ timeout }\\
			\midrule
				\mintinline{c}{ timer_start }\\
				\mintinline{c}{ udt_send(sndpkt) }\\
			\end{tabular}
		} ()
		edge [loop below,below left] node {
			\begin{tabular}{l}
				\mintinline{c}{ rdt_rcv(rcvpkt) }\\
				\mintinline{c}{ && (isSYN(rcvpkt) }\\
				\mintinline[escapeinside=]{c}{ || !isACK(rcvpkt)) }\\
			\midrule
				\multicolumn{1}{c}{ $\bigwedge$ }
			\end{tabular}
		} ()
		edge [] node {
			\begin{tabular}{l}
				\mintinline{c}{ rdt_rcv(rcvpkt) }\\
				\mintinline{c}{ && isFIN(rcvpkt) }\\
				\mintinline{c}{ && isACK(rcvpkt) }\\
			\midrule
				\mintinline{c}{ dequeue(unackedQ) }\\
				\mintinline{c}{ timer_stop }\\
			\end{tabular}
		} (wait)

		(wait) edge [loop above] node {
			\begin{tabular}{l}
				\mintinline{c}{ rdt_rcv(rcvpkt) }\\
			\midrule
				\mintinline{c}{ new_base = ackof(rcvpkt) + 1 }\\
				\mintinline{c}{ n_acked = new_base - base }\\
				\mintinline{c}{ while (n_acked--) }\\
				\mintinline{c}{ |\qquad|dequeue(unackedQ) }\\
				\mintinline{c}{ timer_stop }\\
				\mintinline{c}{ if (base != nextseqnum) }\\
				\mintinline{c}{ |\qquad|timer_start }\\
			\end{tabular}
		} ()
		edge [loop right] node {
			\begin{tabular}{l}
				\mintinline{c}{ rdt_send(data) }\\
			\midrule
				\mintinline{c}{ sndpkt = make_pkt(nextseqnum, data) }\\
				\mintinline{c}{ if (nextseqnum < base + N) |\{| }\\
				\mintinline{c}{ |\qquad|udt_send(sndpkt) }\\
				\mintinline{c}{ |\qquad|enqueue(unackedQ, sndpkt) }\\
				\mintinline{c}{ |\qquad|if (base == nextseqnum) }\\
				\mintinline{c}{ |\qquad\qquad|timer_start }\\
				\mintinline{c}{ |\}| else |\{| }\\
				\mintinline{c}{ |\qquad|enqueue(tosendQ, sndpkt) }\\
				\mintinline{c}{ |\}| }\\
				\mintinline{c}{ nextseqnum++ }\\
				\\
				\\
			\end{tabular}
		} ()
		edge [loop below,below right] node {
			\begin{tabular}{l}
				\mintinline{c}{ timeout }\\
			\midrule
				\mintinline{c}{ timer_start }\\
				\mintinline{c}{ if (!isEmpty(unackedQ)) |\{| }\\
				\mintinline{c}{ |\qquad|for pkt in unackedQ }\\
				\mintinline{c}{ |\qquad\qquad|udt_send(pkt) }\\
				\mintinline{c}{ |\}| else if (!isEmpty(tosendQ)) |\{| }\\
				\mintinline{c}{ |\qquad|while (!isEmpty(tosendQ) && len(unackedQ) <= N) |\{| }\\
				\mintinline{c}{ |\qquad\qquad|sndpkt = dequeue(tosendQ) }\\
				\mintinline{c}{ |\qquad\qquad|udt_send(sndpkt) }\\
				\mintinline{c}{ |\qquad\qquad|enqueue(unackedQ, sndpkt) }\\
				\mintinline{c}{ |\qquad\}| }\\
				\mintinline{c}{ |\}| }\\
			\end{tabular}
		} ()
		edge [swap,bend right] node {
			\begin{tabular}{l}
				\mintinline{c}{ close() }\\
			\midrule
				\mintinline{c}{ sndpkt = make_pkt(nextseqnum, FIN) }\\
				\mintinline{c}{ if (nextseqnum < base + N) |\{|  }\\
				\mintinline{c}{ |\qquad|udt_send(sndpkt) }\\
				\mintinline{c}{ |\qquad|enqueue(unackedQ, sndpkt) }\\
				\mintinline{c}{ |\qquad|if (base == nextseqnum) }\\
				\mintinline{c}{ |\qquad\qquad|timer_start }\\
				\mintinline{c}{ |\}| else |\{| }\\
				\mintinline{c}{ |\qquad|enqueue(tosendQ, sndpkt) }\\
				\mintinline{c}{ |\}| }\\
				\mintinline{c}{ nextseqnum++ }\\
				\mintinline{c}{ sentFIN = true }\\
				\mintinline{c}{ if (receivedFIN) }\\
				\mintinline{c}{ |\qquad|notify_FIN_sent() }\\
			\end{tabular}
		} (wait_for_ack)
		(wait_for_ack) edge [bend left] node {
			\begin{tabular}{l}
				\mintinline{c}{ rdt_rcv(rcvpkt) }\\
				\mintinline{c}{ && hasacknum(rcvpkt, finseqnum) }\\
				\mintinline{c}{ && !receivedFIN }\\
			\midrule
				\multicolumn{1}{c}{ $\bigwedge$ }\\
			\end{tabular}
		} (wait_for_fin)
		edge [loop left] node {
			\begin{tabular}{l}
				\mintinline{c}{ timeout }\\
			\midrule
				\mintinline{c}{ timer_start }\\
				\mintinline{c}{ udt_send(sndpkt) }\\
			\end{tabular}
		} ()
		edge [loop right] node {
			\begin{tabular}{l}
				\mintinline{c}{ rdt_rcv(rcvpkt) }\\
			\midrule
				\mintinline{c}{ new_base = ackof(rcvpkt) + 1 }\\
				\mintinline{c}{ n_acked = new_base - base }\\
				\mintinline{c}{ while (n_acked--) }\\
				\mintinline{c}{ |\qquad|dequeue(unackedQ) }\\
				\mintinline{c}{ timer_stop }\\
				\mintinline{c}{ if (nextseqnum != base) }\\
				\mintinline{c}{ |\qquad|timer_start }\\
			\end{tabular}
		} ()
		edge [bend left=75,swap] node {
			\begin{tabular}{l}
				\mintinline{c}{ rdt_rcv(rcvpkt) }\\
				\mintinline{c}{ && hasacknum(rcvpkt, finseqnum) }\\
				\mintinline{c}{ && !receivedFIN }\\
			\midrule
				\multicolumn{1}{c}{ $\bigwedge$ }\\
			\end{tabular}
		} (shutdown)
		(wait_for_fin) edge [] node {
			\begin{tabular}{l}
				\mintinline{c}{ notify_FIN_received() }\\
			\midrule
				\multicolumn{1}{c}{ $\bigwedge$ }\\
			\end{tabular}
		} (shutdown)
		;
	\end{tikzpicture}
	\caption{Diagram EFSM nadajnika klienta}
	\label{fig:ClientSender}
\end{figure}
\begin{figure}
	\centering
	\begin{tikzpicture}[shorten >=1pt,node distance=9cm,auto,scale=0.65, every node/.style={transform shape}]

		\tikzstyle{every state}=[fill={rgb:black,1;white,10},text width=1.5cm,text centered]

		\node[state,initial left,initial text={ 
			\begin{tabular}{l}
				\multicolumn{1}{c}{ $\bigwedge$ } \\
			\midrule
				\mintinline{c}{ expectedseqnum = 0 } \\
				\mintinline{c}{ dest_port = server_socket_port } \\
				\\
			\end{tabular}
		}]			(wait_for_synack)					{ Wait for \verb_SYNACK_ };
		\node[state]		(wait)			[right of=wait_for_synack]	{ Wait };
		\node[state]		(wait_for_timeout)	[right of=wait]			{ Wait for timeout };
		\node[state]		(wait_for_close)	[below of=wait]			{ Wait for \verb_close()_ };
		\node[state]		(shutdown)		[below of=wait_for_timeout]	{ Shutdown };

		\path[->]
		(wait_for_synack) edge [bend left] node { 
			\begin{tabular}{l}
				\mintinline{c}{ rdt_rcv(rcvpkt) } \\
				\mintinline{c}{ && isSYN(rcvpkt) } \\
				\mintinline{c}{ && isACK(rcvpkt) } \\
			\midrule
				\mintinline{c}{ expectedseqnum = seqof(rcvpkt) } \\
				\mintinline{c}{ sndpkt = make_pkt(expectedseqnum, ACK) } \\
				\mintinline{c}{ expectedseqnum++ } \\
				\mintinline{c}{ extract(rcvpkt, data) } \\
				\mintinline{c}{ dest_port = data } \\
				\mintinline{c}{ udt_send(sndpkt) } \\
				\\
			\end{tabular}
		} (wait)
		(wait) edge [bend left] node {
			\begin{tabular}{l}
				\mintinline{c}{ rdt_rcv(rcvpkt) } \\
				\mintinline{c}{ && hasseqnum(rcvpkt, expectedseqnum) } \\
				\mintinline{c}{ && isFIN(rcvpkt) } \\
				\mintinline{c}{ && sentFIN } \\
			\midrule
				\mintinline{c}{ start_teardown_timer() } \\
				\mintinline{c}{ sndpkt = make_pkt(expectedseqnum, ACK) } \\
				\mintinline{c}{ udt_send(sndpkt) } \\
				\mintinline{c}{ expectedseqnum++ } \\
				\mintinline{c}{ receivedFIN = true } \\
				\mintinline{c}{ notify_FIN_received() } \\
				\\
			\end{tabular}
		} (wait_for_timeout)
		edge [left,bend right] node {
			\begin{tabular}{l}
				\mintinline{c}{ rdt_rcv(rcvpkt) } \\
				\mintinline{c}{ && hasseqnum(rcvpkt, expectedseqnum) } \\
				\mintinline{c}{ && isFIN(rcvpkt) } \\
				\mintinline{c}{ && !sentFIN } \\
			\midrule
				\mintinline{c}{ sndpkt = make_pkt(expectedseqnum, ACK) } \\
				\mintinline{c}{ udt_send(sndpkt) } \\
				\mintinline{c}{ expectedseqnum++ } \\
				\mintinline{c}{ receivedFIN = true } \\
			\end{tabular}
		} (wait_for_close)
		edge [loop left] node {
			\begin{tabular}{l}
				\multicolumn{1}{c}{\mintinline{c}{ default }} \\
			\midrule
				\mintinline{c}{ udt_send(sndpkt) }
			\end{tabular}
		} ()
		edge [loop below,below right] node {
			\begin{tabular}{l}
				\mintinline{c}{ rdt_rcv(rcvpkt) } \\
				\mintinline{c}{ && hasseqnum(rcvpkt, expectedseqnum) } \\
			\midrule
				\mintinline{c}{ extract(rcvpkt, data) } \\
				\mintinline{c}{ deliver_data(data) } \\
				\mintinline{c}{ sndpkt = make_pkt(expectedseqnum, ACK) } \\
				\mintinline{c}{ udt_send(sndpkt) } \\
				\mintinline{c}{ expectedseqnum++ } \\
			\end{tabular}
		} ()
		(wait_for_timeout) edge [bend left] node {
			\begin{tabular}{c}
				\mintinline{c}{ teardown_timer_timeout } \\
			\midrule
				$\bigwedge$ \\
			\end{tabular}
		} (shutdown)
		edge [loop right] node {
			\begin{tabular}{l}
				\multicolumn{1}{c}{\mintinline{c}{ default }} \\
			\midrule
				\mintinline{c}{ udt_send(sndpkt) }
			\end{tabular}
		} ()
		(wait_for_close) edge [loop left] node {
			\begin{tabular}{l}
				\multicolumn{1}{c}{\mintinline{c}{ default }} \\
			\midrule
				\mintinline{c}{ udt_send(sndpkt) }
			\end{tabular}
		} ()
		edge [bend right] node {
			\begin{tabular}{c}
				\mintinline{c}{ notify_FIN_sent() } \\
			\midrule
				$\bigwedge$
			\end{tabular}
		} (shutdown)
		;
	\end{tikzpicture}
	\caption{Diagram EFSM odbiornika klienta}
	\label{fig:ClientReceiver}
\end{figure}
\begin{figure}
	\centering
	\begin{tikzpicture}[shorten >=1pt,node distance=9cm,auto,scale=0.65, every node/.style={transform shape}]

		\tikzstyle{every state}=[fill={rgb:black,1;white,10},text width=1.5cm,text centered]

		\node[state,initial,initial text={ 
			\begin{tabular}{l}
				\multicolumn{1}{c}{ $\bigwedge$ }\\
			\midrule
				\mintinline{c}{ expectedseqnum = n1 + 1 }\\
			\end{tabular}
		}]			(unhandshaken)						{ Not initialised };
		\node[state]		(wait)			[right of=unhandshaken]		{ Wait };
		\node[state]		(wait_for_timeout)	[right of=wait]			{ Wait for timeout };
		\node[state]		(wait_for_close)	[below of=wait]			{ Wait for \verb_close()_ };
		\node[state]		(shutdown)		[below of=wait_for_timeout]	{ Shutdown };

		\path[->]
		(unhandshaken) edge [bend left] node { 
			\begin{tabular}{c}
				\mintinline{c}{ nofify_handshake() }\\
			\midrule
				$\bigwedge$ \\
			\end{tabular}
		} (wait)
		(wait) edge [bend left] node {
			\begin{tabular}{l}
				\mintinline{c}{ rdt_rcv(rcvpkt) } \\
				\mintinline{c}{ && hasseqnum(rcvpkt, expectedseqnum) } \\
				\mintinline{c}{ && isFIN(rcvpkt) } \\
				\mintinline{c}{ && sentFIN } \\
			\midrule
				\mintinline{c}{ start_teardown_timer() } \\
				\mintinline{c}{ sndpkt = make_pkt(expectedseqnum, ACK) } \\
				\mintinline{c}{ udt_send(sndpkt) } \\
				\mintinline{c}{ expectedseqnum++ } \\
				\mintinline{c}{ receivedFIN = true } \\
				\mintinline{c}{ notify_FIN_received() } \\
				\\
			\end{tabular}
		} (wait_for_timeout)
		edge [left,bend right] node {
			\begin{tabular}{l}
				\mintinline{c}{ rdt_rcv(rcvpkt) } \\
				\mintinline{c}{ && hasseqnum(rcvpkt, expectedseqnum) } \\
				\mintinline{c}{ && isFIN(rcvpkt) } \\
				\mintinline{c}{ && !sentFIN } \\
			\midrule
				\mintinline{c}{ sndpkt = make_pkt(expectedseqnum, ACK) } \\
				\mintinline{c}{ udt_send(sndpkt) } \\
				\mintinline{c}{ expectedseqnum++ } \\
				\mintinline{c}{ receivedFIN = true } \\
			\end{tabular}
		} (wait_for_close)
		edge [loop left] node {
			\begin{tabular}{l}
				\multicolumn{1}{c}{\mintinline{c}{ default }} \\
			\midrule
				\mintinline{c}{ udt_send(sndpkt) }
			\end{tabular}
		} ()
		edge [loop below,below right] node {
			\begin{tabular}{l}
				\mintinline{c}{ rdt_rcv(rcvpkt) } \\
				\mintinline{c}{ && hasseqnum(rcvpkt, expectedseqnum) } \\
			\midrule
				\mintinline{c}{ extract(rcvpkt, data) } \\
				\mintinline{c}{ deliver_data(data) } \\
				\mintinline{c}{ sndpkt = make_pkt(expectedseqnum, ACK) } \\
				\mintinline{c}{ udt_send(sndpkt) } \\
				\mintinline{c}{ expectedseqnum++ } \\
			\end{tabular}
		} ()
		(wait_for_timeout) edge [bend left] node {
			\begin{tabular}{c}
				\mintinline{c}{ teardown_timer_timeout } \\
			\midrule
				$\bigwedge$ \\
			\end{tabular}
		} (shutdown)
		edge [loop right] node {
			\begin{tabular}{l}
				\multicolumn{1}{c}{\mintinline{c}{ default }} \\
			\midrule
				\mintinline{c}{ udt_send(sndpkt) }
			\end{tabular}
		} ()
		(wait_for_close) edge [loop left] node {
			\begin{tabular}{l}
				\multicolumn{1}{c}{\mintinline{c}{ default }} \\
			\midrule
				\mintinline{c}{ udt_send(sndpkt) }
			\end{tabular}
		} ()
		edge [bend right] node {
			\begin{tabular}{c}
				\mintinline{c}{ notify_FIN_sent() } \\
			\midrule
				$\bigwedge$
			\end{tabular}
		} (shutdown)
		;
	\end{tikzpicture}
	\caption{Diagram EFSM nadajnika serwera}
	\label{fig:ServerSender}
\end{figure}
\begin{figure}
	\centering
	\begin{tikzpicture}[shorten >=1pt,node distance=11cm,auto,scale=0.65, every node/.style={transform shape}]

		\tikzstyle{every state}=[fill={rgb:black,1;white,10},text width=1.5cm,text centered]

		\node[state,initial left,initial text={ 
			\begin{tabular}{l}
				\multicolumn{1}{c}{ $\bigwedge$ } \\
			\midrule
				\mintinline{c}{ nextseqnum = n2 + 1 } \\
				\mintinline{c}{ base = n2 + 1 } \\
				\mintinline{c}{ unackedQ = {} } \\
				\mintinline{c}{ tosendQ = {} } \\
				\mintinline{c}{ target_port = client_port } \\
				\mintinline{c}{ send_FIN = false } \\
			\end{tabular}
		}]			(unhandshaken)						{ Not initialised };
		\node[state]		(wait)			[right of=unhandshaken]		{ Wait };
		\node[state]		(wait_for_ack)		[right of=wait]			{ Wait for \verb_ACK_ };
		\node[state]		(wait_for_fin)		[below of=wait]			{ Wait for \verb_FIN_ };
		\node[state]		(shutdown)		[below of=wait_for_ack]		{ Shutdown };

		\path[->]
		(unhandshaken) edge [] node { 
			\begin{tabular}{l}
				\mintinline{c}{ rdt_rcv(rcvpkt) && isACK(rcvpkt) } \\
				% @TODO: vefify the following line
				\mintinline{c}{ && ackof(rcvpkt) == nextseqnum - 1 } \\
			\midrule
				\mintinline{c}{ notify_handshake() } \\
			\end{tabular}
		} (wait)
		(wait) edge [loop above,above left] node {
			\begin{tabular}{l}
				\mintinline{c}{ rdt_rcv(rcvpkt) } \\
			\midrule
				\mintinline{c}{ new_base = ackof(rcvpkt) + 1 } \\
				\mintinline{c}{ n_acked = new_base - base } \\
				\mintinline{c}{ while (n_acked--) } \\
				\mintinline{c}{ |\qquad|dequeue(unackedQ) } \\
				\mintinline{c}{ timer_stop } \\
				\mintinline{c}{ if (base != nextseqnum) } \\
				\mintinline{c}{ |\qquad|timer_start } \\
			\end{tabular}
		} ()
		edge [loop right] node {
			\begin{tabular}{l}
				\\
				\\
				\\
				\\
				\\
				\\
				\mintinline{c}{ rdt_send(data) } \\
			\midrule
				\mintinline{c}{ sndpkt = make_pkt(nextseqnum, data) } \\
				\mintinline{c}{ if (nextseqnum < base + N) |\{| } \\
				\mintinline{c}{ |\qquad|udt_send(sndpkt) } \\
				\mintinline{c}{ |\qquad|enqueue(unackedQ, sndpkt) } \\
				\mintinline{c}{ |\qquad|if (base == nextseqnum) } \\
				\mintinline{c}{ |\qquad\qquad|timer_start } \\
				\mintinline{c}{ |\}| else |\{| } \\
				\mintinline{c}{ |\qquad|enqueue(tosendQ, sndpkt) } \\
				\mintinline{c}{ |\}| } \\
				\mintinline{c}{ nextseqnum++ } \\
			\end{tabular}
		} ()
		edge [loop below,below left] node {
			\begin{tabular}{l}
				\mintinline{c}{ timeout }\\
			\midrule
				\mintinline{c}{ timer_start }\\
				\mintinline{c}{ if (!isEmpty(unackedQ)) |\{| }\\
				\mintinline{c}{ |\qquad|for pkt in unackedQ }\\
				\mintinline{c}{ |\qquad\qquad|udt_send(pkt) }\\
				\mintinline{c}{ |\}| else if (!isEmpty(tosendQ)) |\{| }\\
				\mintinline{c}{ |\qquad|while (!isEmpty(tosendQ) && }\\
				\mintinline{c}{ |\qquad\qquad\quad|len(unackedQ) <= N) |\{| }\\
				\mintinline{c}{ |\qquad\qquad|sndpkt = dequeue(tosendQ) }\\
				\mintinline{c}{ |\qquad\qquad|udt_send(sndpkt) }\\
				\mintinline{c}{ |\qquad\qquad|enqueue(unackedQ, sndpkt) }\\
				\mintinline{c}{ |\qquad\}| }\\
				\mintinline{c}{ |\}| }\\
			\end{tabular}
		} ()
		edge [bend left=40] node {
			\begin{tabular}{l}
				\mintinline{c}{ close() } \\
			\midrule
				\mintinline{c}{ sndpkt = make_pkt(nextseqnum, FIN) } \\
				\mintinline{c}{ if (nextseqnum < base + N) |\{|  } \\
				\mintinline{c}{ |\qquad|udt_send(sndpkt) } \\
				\mintinline{c}{ |\qquad|enqueue(unackedQ, sndpkt) } \\
				\mintinline{c}{ |\qquad|if (base == nextseqnum) } \\
				\mintinline{c}{ |\qquad\qquad|timer_start } \\
				\mintinline{c}{ |\}| else |\{| } \\
				\mintinline{c}{ |\qquad|enqueue(tosendQ, sndpkt) } \\
				\mintinline{c}{ |\}| } \\
				\mintinline{c}{ nextseqnum++ } \\
				\mintinline{c}{ sentFIN = true } \\
				\mintinline{c}{ if (receivedFIN) } \\
				\mintinline{c}{ |\qquad|notify_FIN_sent() } \\
			\end{tabular}
		} (wait_for_ack)
		(wait_for_ack) edge [] node {
			\begin{tabular}{l}
				\mintinline{c}{ rdt_rcv(rcvpkt) } \\
				\mintinline{c}{ && hasacknum(rcvpkt, finseqnum) }\\
				\mintinline{c}{ && !receivedFIN }\\
			\midrule
				\multicolumn{1}{c}{ $\bigwedge$ } \\
			\end{tabular}
		} (wait_for_fin)
		edge [loop above] node {
			\begin{tabular}{l}
				\mintinline{c}{ timeout } \\
			\midrule
				\mintinline{c}{ timer_start } \\
				\mintinline{c}{ udt_send(sndpkt) } \\
			\end{tabular}
		} ()
		edge [loop right] node {
			\begin{tabular}{l}
				\mintinline{c}{ rdt_rcv(rcvpkt) } \\
			\midrule
				\mintinline{c}{ new_base = ackof(rcvpkt) + 1 } \\
				\mintinline{c}{ n_acked = new_base - base } \\
				\mintinline{c}{ while (n_acked--) } \\
				\mintinline{c}{ |\qquad|dequeue(unackedQ) } \\
				\mintinline{c}{ timer_stop } \\
				\mintinline{c}{ if (nextseqnum != base) } \\
				\mintinline{c}{ |\qquad|timer_start } \\
			\end{tabular}
		} ()
		edge [bend left] node {
			\begin{tabular}{l}
				\mintinline{c}{ rdt_rcv(rcvpkt) } \\
				\mintinline{c}{ && hasacknum(rcvpkt, finseqnum) }\\
				\mintinline{c}{ && !receivedFIN }\\
			\midrule
				\multicolumn{1}{c}{ $\bigwedge$ } \\
			\end{tabular}
		} (shutdown)
		(wait_for_fin) edge [bend right] node {
			\begin{tabular}{l}
				\mintinline{c}{ notify_FIN_received() } \\
			\midrule
				\multicolumn{1}{c}{ $\bigwedge$ } \\
			\end{tabular}
		} (shutdown)
		;
	\end{tikzpicture}
	\caption{Diagram EFSM odbiornika serwera}
	\label{fig:ServerReceiver}
\end{figure}
\end{landscape}

\section{Opis warstwy aplikacji}
\subsection{Opis stanów użytkowników}
Użytkownik może znajdować się w jednym z dwóch stanów:
\begin{itemize}
	\item aktywnym, czyli być zarejestrowanym na serwerze, oraz
	\item nieaktywnym, czyli nie być~zarejestrowanym na serwerze.
\end{itemize}
Użytkownicy nieaktywni mogą czatować z aktywnymi. Sprawia to, że o ile znamy
tożsamość użytkownika z którym chcemy się porozumieć, użycie serwera staje się
opcjonalne.

\subsection{Opis usług}
Usługi, które serwer musi udostępniać klientom to:
\begin{enumerate}
	\item Możliwość określenia się jako aktywny użytkownik,
	\item Możliwość określenia się~jako nieaktywny użytkownik, oraz
	\item Możliwość pozyskania informacji o aktywnych użytkownikach.
\end{enumerate}
Usługi, które muszą udostępniać sobie nawzajem klienci:
\begin{enumerate}
	\item Możliwość wymiany wiadomości.
\end{enumerate}

\subsection{Opis serwera}
Serwer powinien zawierać informację o aktualnie aktywnych klientach i
móc udostępniać je użytkownikom, którzy chcieliby rozpoczynać między
sobą rozmowy. Strukturę danych zawierają informację o pojedynczym
kliencie nazwiemy ``ClientRecord''.
Podstawowym zbiorem informacji (zapewniającym minimalną funkcjonalność
klientom) jest para \texttt{(ip,\ port)}. Klient, który otrzyma takie
informacje jest w stanie jednoznacznie wskazać rozmówcę, chociaż nie
jest to pewnie wygodne dla użytkownika.
Aby uprościć używanie klienta, do ClientRecord dodamy pole ``username''.
Pozwoli to użytkownikom na rozróżnienie pomiędzy rozmówcami w sposób
prostszy niż zapamiętywanie numerów portów i IP. Klienta będzie zatem
identyfikować trójka \texttt{(ip,\ port,\ username)}. Użytkownikom
(przez klienta) będzie to prezentowane w formie:
\begin{verbatim}
<display_nick> ::= <username> '#' <ip_port_hash>
\end{verbatim}
Gdzie \texttt{\textless{}ip\_port\_hash\textgreater{}} jest
czterocyfrową liczbą heksadecymalną będącą sumą w uzupełnieniu do
jedynek 16 bitowych słów konkatenacji ip (32 bity) oraz portu (16
bitów). Sposób liczenia haszu to tak zwana `suma internetowa', którą
opisuje {[}RFC 1071{]}.

Poprzedzający schemat dostarcza usługi nr. 1 - pozwala na określanie się
przez użytkowników jako `aktywni'. Usługę nr. 2 zrealizujemy przez
dodanie czasu TTL do \texttt{ClientRecord}, oraz przez możliwość
wysłania rządania o usunięcie z rejestru.
Czas TTL będzie ustalany przez klienta i akceptowany przez serwer. W
celu uniknięcia sytuacji, w której klient może zarejestrować się na
bardzo długi czas, po czym rozłączyć bez wysłania rządania usunięcia, będzie on
ograniczony do wartości maksymalnej 10 min. Będzie to jednocześnie wartość
domyślna, jeżeli klient jej nie określi.
Żądanie o usunięcie będzie musiało uwzględniać schemat autoryzacji.
Inaczej dowolny klient mógłby opróżnić rejestr. Oryginalnie miał on
opierać się o kryptografię asymetryczną, jednak z braku czasu zostanie
zastąpiony prostym hasłem.
Uwzględniwszy powyższą realizację usługi nr. 2, ClientRecord zostanie
wzbogacony o pola \texttt{ttl} oraz \texttt{password}.

Usługa trzecia realizowana będzie poprzez rządanie
\texttt{GET\ R\_0\ N}, która zwróci N rekordów z rejestru, od
\texttt{R\_0} do \texttt{R\_N-1} oraz możliwość wyszukiwania.
Wyszukiwanie będzie odbywać się poprzez wysłanie rządania z adresem
hosta, portem lub nazwą użytkownika. Informacje te będą porównywane z
informacjami w rejestrze i zwracane będą~wszystkie rekordy których pola
pokrywają się z argumentami wyszukiwania.
Z racji, że poprawnym składniowo jest nie zawarcie argumentów
wyszukiwania, serwer w tej sytuacji powinien zwrócić pustą listę
wyników.

Protokół komunikacyjny klient-serwer jest protokołem bezstanowym. \\
Schemat rekordu użytkownika:
\begin{verbatim}
ClientRecord = (ip, port, username, ttl)
\end{verbatim}
\begin{verbatim}
<client_record> ::= <ip> '␣' <port> '␣' <username> '␣' <ttl> '\r\n'
\end{verbatim}

\subsubsection{Wiadomości komunikacji serwera}
Rządanie, format ogólny:
\begin{verbatim}
<request> ::= <operation> '␣' <arguments> '\r\n'
\end{verbatim}
Rządania rejestracji:
\begin{verbatim}
<register_request> ::= 
    'REGISTER' '␣' <username> '␣' <port> '␣' <password> '␣' <ttl> '\r\n'
<unregister_request> ::= 
    'UNREGISTER' '␣' <username> '␣' <password> '␣' <port> '\r\n\'
\end{verbatim}
Rządania przeszukiwania:
\begin{verbatim}
<client_record_request> ::=
    'GET' '␣' <starting_index> '␣' <ending_index> '\r\n'
<client_record_search_request> ::= 
    'SEARCH' '␣' <search_arguments> '\r\n'
<search_arguments> ::= [<ip> '␣'] [<port> '␣'] [<username>]
\end{verbatim}
Odpowiedź, format ogólny:
\begin{verbatim}
<response> ::= <status> '\r\n' <client_records>
<client_records> ::= <client_record> '\r\n' [<client_records>] | '\r\n'
\end{verbatim}
Należy zaznaczyć, że składania dopuszcza brak rekordów użytkowników w
odpowiedzi.\\
Odpowiedzi rejestracji:
\begin{verbatim}
<register_response> ::= <response> '\r\n'
<unregister_response> ::= <response> '\r\n'
<response> ::= 'OK' | 'PASSWD_ERR' | 'SERVER_ERR'
	       | 'INVALID_REQUEST' | 'USER_NOT_FOUND'
\end{verbatim}
Odpowiedzi przeszukiwania:
\begin{verbatim}
<search_response> ::= 'OK' '\r\n' <client_records> | 'SERVER_ERR' '\r\n'
\end{verbatim}

\subsection{Opis klientów}
Klienci mają możliwość wyboru rozmówcy oraz wymiany wiadomości
tekstowych.
Aby odnaleźć rozmóców, użytkownik podaje ip serwera zawierającego
rejestr użytkowników przy starcie aplikacji, lub w pliku
konfiguracyjnym. Następnie może przeszukiwać bazę danych serwera poprzez
podanie adresu komputera, portu lub nazwy użytkownika.
Po pobraniu danych z serwera, użytkownik może rozpocząć
konwersację. 

Wiadomości protokołu nie zawierają informacji o tym  do kogo
wysyłana jest wiadomość, ponieważ jednoznacznie określa to połączenie. 
Wiadomości zawierają numery gniazd nasłuchujących nadawców, ponieważ są
przesyłane po jednorazowych połączeniach, których numery gniazd są różne od
numeru gniazda nasłuchującego zawartego w rekordzie użytkownika. W innym
przypadku połączenie nie niosłoby wystarczająco informacji, aby określić
nadawcę. Znaczniki czasowe wiadomości są zawsze w strefie czasowej UTC.
Protokół komunikacji między użytkownikami również jest bezstanowy.

\subsubsection{Wiadomości komunikacji klienta}
Wiadomość tekstowa:
\begin{verbatim}
<text_message> ::= 
    'MESSAGE' '␣' <sender_listen_port> '␣' <timestamp> '\r\n' <message_body> '\r\n\r\n'
\end{verbatim}
Plik:
\begin{verbatim}
<file_attachment> ::=
    'FILE' '␣' <sender_listen_port> '␣' <timestamp> '\r\n' <file_in_base64> '\r\n\r\n'
\end{verbatim}
Timestamp, w UTC:
\begin{verbatim}
<timestamp> ::= 
    <year> '/' <day> '/' <month> '/' <hour> ':' <minute> ':' <second>
\end{verbatim}
\end{document}

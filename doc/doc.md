Opis stanów użytkowników:
================================================================================
Użytkownik może znajdować się w jednym z dwóch stanów:

- aktywnym, czyli być zarejestrowanym na serwerze, oraz
- nieaktywnym, czyli nie być zarejestrowanym na serwerze.

Użytkownicy nieaktywni mogą stale czatować z aktywnymi.

Opis usług:
================================================================================
Usługi, które serwer musi udostępniać klientom to:

1. Możliwość określenia się jako aktywny użytkownik,
2. Możliwość określenia się jako nieaktywny użytkownik, oraz
3. Możliwość pozyskania informacji o aktywnych użytkownikach.

Usługi, które muszą udostępniać sobie nawzajem klienci:

1. Możliwość wymiany wiadomości.

Opis serwera:
================================================================================
Serwer powinien zawierać informację o aktualnie aktywnych klientach i móc
udostępniać je użytkownikom, którzy chcieliby rozpoczynać między sobą rozmowy.
Strukturę danych zawierają informację o pojedynczym kliencie nazwiemy
"ClientRecord".

Podstawowym zbiorem informacji (zapewniającym minimalną funkcjonalność
klientom) jest para `(ip, port)`. Klient, który otrzyma takie informacje
jest w stanie jednoznacznie wskazać rozmówcę, chociaż nie jest to pewnie wygodne
dla użytkownika.

Aby uprościć używanie klienta, do ClientRecord dodamy pole "username". Pozwoli
to użytkownikom na rozróżnienie pomiędzy rozmówcami w sposób prostszy niż
zapamiętywanie numerów portów i IP. Klienta będzie zatem identyfikować trójka
`(ip, port, username)`. Użytkownikom (przez klienta) będzie to prezentowane w
formie:

	<display_nick> ::= <username> '#' <ip_port_hash>

Gdzie `<ip_port_hash>` jest czterocyfrową liczbą heksadecymalną będącą sumą w
uzupełnieniu do jedynek 16 bitowych słów konkatenacji ip (32 bity) oraz portu
(16 bitów). Sposób liczenia haszu to tak zwana 'suma internetowa', którą opisuje
[RFC 1071].

Poprzedzający schemat dostarcza usługi nr. 1 - pozwala na określanie się przez
użytkowników jako 'aktywni'. Usługę nr. 2 zrealizujemy przez dodanie czasu TTL
do `ClientRecord`, oraz przez możliwość wysłania rządania o usunięcie z rejestru.

Czas TTL będzie ustalany przez klienta i akceptowany przez serwer. W celu
uniknięcia sytuacji, w której klient może zarejestrować się na bardzo długi
czas, po czym rozłączyć, będzie on ograniczony do wartości maksymalnej 10 min.
Będzie to jednocześnie wartość domyślna, jeżeli klient jej nie określi.

Żądanie o usunięcie będzie musiało uwzględniać schemat autoryzacji. Inaczej
dowolny klient mógłby opróżnić rejestr. Oryginalnie miał on opierać się o
kryptografię asymetryczną, jednak z braku czasu zostanie zastąpiony prostym
hasłem.

Uwzględniwszy powyższą realizację usługi nr. 2, ClientRecord zostanie wzbogacony
o pola `ttl` oraz `password`.

Usługa trzecia realizowana będzie poprzez rządanie `GET R_0 N`, która zwróci N
rekordów z rejestru, od `R_0` do `R_N-1` oraz możliwość wyszukiwania.

Wyszukiwanie będzie odbywać się poprzez wysłanie rządania z adresem hosta,
portem lub nazwą użytkownika. Informacje te będą porównywane z informacjami w
rejestrze i zwracane będą wszystkie rekordy których pola pokrywają się z
argumentami wyszukiwania.

Z racji, że poprawnym składniowo jest nie zawarcie argumentów wyszukiwania,
serwer w tej sytuacji powinien zwrócić pustą listę wyników.

Protokół komunikacyjny klient-serwer jest protokołem bezstanowym.

Schemat rekordu użytkownika:

	ClientRecord = (ip, port, username, ttl)

Wiadomości komunikacji serwera:
--------------------------------------------------------------------------------
Rządanie, format ogólny:

	<request> ::= <operation> '␣' <arguments> '\r\n'


Rządania rejestracji:

	<register_request> ::= 
		'REGISTER' '␣' <username> '␣' <password> '␣' <ttl> '\r\n'
	<unregister_request> ::= 
		'UNREGISTER' '␣' <username> '␣' <password> '␣' <ttl> '\r\n\'

Rządania przeszukiwania:

	<user_record_request> ::=
		'GET' '␣' <starting_index> '␣' <ending_index> '\r\n'
	<user_record_search_request> ::= 
		'SEARCH' '␣' <search_arguments> '\r\n'
	<search_arguments> ::= [<ip> '␣'] [<port> '␣'] [<username>]

Odpowiedź, format ogólny:

	<response> ::= <status> '\r\n' <user_records>
	<user_records> ::= <user_record> '\r\n' [<user_records>] | '\r\n'

Należy zaznaczyć, że składania dopuszcza brak rekordów użytkowników w odpowiedzi.

Odpowiedzi rejestracji:
	
	<register_response> ::= <response> '\r\n'
	<unregister_response> ::= <response> '\r\n'
	<response> ::= 'OK' | 'PASSWD_ERR' | 'SERVER_ERR'

Odpowiedzi przeszukiwania:
	
	<search_response> ::= 'OK' '\r\n' <user_records> | 'SERVER_ERR' '\r\n'


Opis klientów
================================================================================
Klienci mają możliwość wyboru rozmówcy oraz wymiany wiadomości tekstowych. 

Aby odnaleźć rozmóców, użytkownik podaje ip serwera zawierającego rejestr
użytkowników przy starcie aplikacji, lub w pliku konfiguracyjnym. Następnie może
przeszukiwać bazę danych serwera poprzez podanie adresu komputera, portu lub
nazwy użytkownika.

Po pobraniu danych użytkownika z serwera, użytkownik może rozpocząć konwersację.
Wiadomości protokołu nie uwzględniają do kogo wysyłana jest wiadomość, ponieważ
jednoznacznie określa to połączenie.

Znaczniki czasowe wiadomości będą zawsze w strefie czasowej UTC.

Protokół komunikacji między użytkownikami również jest bezstanowy.

Wiadomości komunikacji klienta:
--------------------------------------------------------------------------------
Wiadomość tekstowa:

	<text_message> ::= 
		'MESSAGE' '␣' <timestamp> '\r\n' <message_body> '\r\n'

Plik:

	<file_attachment> ::=
		'MESSAGE' '␣' <timestamp> '\r\n' <file_in_base64> '\r\n'

Timestamp, w UTC:

	<timestamp> ::= <hour> ':' <minute> ':' <second>


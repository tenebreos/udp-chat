package udpchat;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Objects;
import java.util.Scanner;

public class ClientRecord {

    private final InetAddress address;
    private final int port;
    private final String username;
    private long ttl; // s
    private final String tag;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientRecord that = (ClientRecord) o;
        return port == that.port &&
                address.equals(that.address) &&
                username.equals(that.username) &&
                tag.equals(that.tag);
    }

    @Override
    public int hashCode() {
        return Objects.hash(address, port, username, tag);
    }

    public ClientRecord(InetAddress address, int port, String username,
                        long ttl) {
        if (!GlobalRegexes.name_regex.matcher(username).matches())
            throw new IllegalArgumentException("Username may only contain " +
                    "alphanumeric characters and punctuation marks!");
        this.address = address;
        this.port = port;
        this.username = username;
        this.ttl = ttl;
        this.tag = calculateTag(address, port);
    }

    @Override
    public String toString() {
        return String.format("%s %d %s %d",
                address.getHostAddress(), port, username, ttl);
    }

    private String calculateTag(InetAddress address, int port) {
        byte[] addr = address.getAddress();
        ByteBuffer buf = ByteBuffer.allocate(addr.length + 4);
        buf.put(addr).putShort((short) port);
        int hash = 0;
        for (int i = 0; i < 3; i++) {
            hash += ((int) buf.getShort(2 * i)) & 0x0000FFFF; // it gets sign extended...
            if (hash > 0xffff) {
                hash &= 0xffff;
                hash += 1;
            }
        }
        return String.format("%04X", hash);
    }

    public String getTag() {
        return tag;
    }

    public InetAddress getAddress() {
        return address;
    }

    public int getPort() {
        return port;
    }

    public long getTTL() {
        return ttl;
    }

    public void updateTTL(long ttl) {
        this.ttl = ttl;
    }

    public String getUsername() {
        return username;
    }

    public String getFullName() {
        return username + "#" + tag;
    }

    public static ClientRecord parseRecord(String record) throws IOException {
        Scanner crs = new Scanner(record);
        InetAddress ip;
        try {
            ip = InetAddress.getByName(crs.next());
        } catch (UnknownHostException x) {
            throw new IOException("Malformed ClientRecord string");
        }
        int port = crs.nextInt();
        String username = crs.next();
        long ttl = crs.nextLong();
        return new ClientRecord(ip, port, username, ttl);
    }
}

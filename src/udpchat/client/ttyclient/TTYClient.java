package udpchat.client.ttyclient;

import udpchat.ClientRecord;
import udpchat.GlobalRegexes;
import udpchat.client.ChatMessage;
import udpchat.client.ClientConnector;
import udpchat.client.RegistryServerConnector;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class TTYClient {

    private enum Choice { CONVERSE, SEARCH }
    private final Map<ClientRecord, ArrayList<ChatMessage>> messages = new HashMap<>();
    private RegistryServerConnector server;
    private ClientRecord identity;
    private int peer2PeerPort = 2137;

    public TTYClient(InetAddress serverAddress, int serverPort) {
        server = new RegistryServerConnector(serverAddress, serverPort);
    }

    public void repl() throws IOException {
        final String prompt = "$ ";
        boolean loop = true;
        try (BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in))) {
outer:
            do {
                System.out.print(prompt);
                String line = stdin.readLine();
                if (line == null)
                    break;
                Scanner s = new Scanner(line);
                String command = s.next();
                switch (command) {
                    case "r":
                        register(s.next(GlobalRegexes.name_regex));
                        break;
                    case "u":
                        unregister();
                        break;
                    case "s": {
                        InetAddress ip = null;
                        int port = 0;
                        String name = null;
                        int n_fields = 0;
                        while (s.hasNext()) {
                            if (n_fields > 3) {
                                System.out.println("Usage: s [ip] [port] [username]");
                                continue outer;
                            }
                            if (s.hasNext(GlobalRegexes.ip_regex)) {
                                try {
                                    ip = InetAddress.getByName(s.next());
                                } catch (UnknownHostException unknownHostException) {
                                    System.out.println("This should not happen");
                                }
                            } else if (s.hasNext(GlobalRegexes.port_regex)) {
                                port = s.nextInt();
                            } else if (s.hasNext(GlobalRegexes.name_regex)) {
                                name = s.next();
                            }
                            n_fields++;
                        }
                        if (n_fields == 0) {
                            System.out.println("Usage: s [ip] [port] [username]");
                            continue outer;
                        }
                        search(ip, port, name);
                    } break;
                    case "g": {
                        int n1 = s.nextInt();
                        int n2 = s.nextInt();
                        get(n1, n2);
                    } break;
                    case "c": {
                    } break;
                    case "l": {
                        ArrayList<ClientRecord> recs = new ArrayList<>(messages.keySet());
                        displayRecords(recs);
                    } break;
                    case "q":
                        loop = false;
                        break;
                    default:
                        System.out.println("Invalid command");
                        break;
                }
            } while (loop);
        }
    }

    private void search(InetAddress ip, int port, String username) throws IOException {
        ArrayList<ClientRecord> crs = server.search(ip, port, username);
        for (ClientRecord cr : crs) {
            messages.put(cr, new ArrayList<ChatMessage>());
        }
    }

    private void register(String username) throws IOException {
        InetAddress ip = InetAddress.getLocalHost();
        long ttl = 10000;
        identity = new ClientRecord(ip, peer2PeerPort, username, ttl);
        server.register(identity, "asdf");
    }

    private void unregister() throws IOException {
        server.unregister(identity, "asdf");
    }

    private void get(int n1, int n2) throws IOException {
        ArrayList<ClientRecord> crs = server.get(n1, n2);
        crs.forEach(clientRecord -> {
            if (!messages.containsKey(clientRecord))
                messages.put(clientRecord, new ArrayList<ChatMessage>());
        });
    }

    private void displayRecords(ArrayList<ClientRecord> records) {
        int i = 1;
        for (ClientRecord cr : records)
            System.out.format("%d: %s\t%s:%s\n", i++, cr.getFullName(), cr.getAddress(), cr.getPort());
    }

    private void converse(ClientRecord c, ArrayList<ChatMessage> msgs) {
        System.out.format("Conversing with %s.%n", c.getFullName());
        Thread applicationThread = Thread.currentThread();
        Thread screenReader = new Thread() {
            @Override
            public void run() {

            }
        };
        Thread screenWriter = new Thread() {
            @Override
            public void run() {

            }
        };
    }

    public static void main(String[] args) throws Exception {
        if (args.length < 2) {
            System.err.println("Usage: TTYClient <registry_server_ip> <registry_server_port>");
            return;
        }
        InetAddress ip = InetAddress.getByName(args[0]);
        int port = Integer.parseInt(args[1]);

        TTYClient c = new TTYClient(ip, port);
        c.repl();
    }
}

package udpchat.client;

import udpchat.ClientRecord;
import udpchat.GlobalRegexes;

import ttp.TTPSocket;
import ttp.tests.Client;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.regex.Pattern;

import static udpchat.GlobalRegexes.ip_regex;
import static udpchat.GlobalRegexes.name_regex;

public class RegistryServerConnector {

    private final InetAddress registryServerAddress;
    private final int registryServerPort;

    public RegistryServerConnector(InetAddress registryServerAddress,
                                   int registryServerPort) {
        this.registryServerAddress = registryServerAddress;
        this.registryServerPort = registryServerPort;
    }

    public void register(ClientRecord cr, String password) throws IOException {
        try (
                TTPSocket s = new TTPSocket(registryServerAddress, registryServerPort);
                BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
                PrintWriter out = new PrintWriter(new OutputStreamWriter(s.getOutputStream()), true)
        ) {
            out.format("REGISTER %s %d %s %d\r\n", cr.getUsername(), cr.getPort(), password, cr.getTTL());
            out.flush();
            Scanner response = new Scanner(in);
            processResponse(response);
        } catch (IOException x) {
            String errorMessage = "Error fetching client records from a " +
                    "remote server. Check the correctness of provided " +
                    "ip address and/or network connectivity";
            throw new IOException(errorMessage);
        }
    }

    public void unregister(ClientRecord cr, String password) throws IOException, IllegalArgumentException {
        try (
                TTPSocket s = new TTPSocket(registryServerAddress, registryServerPort);
                BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
                PrintWriter out = new PrintWriter(new OutputStreamWriter(s.getOutputStream()), true)
        ) {
            out.format("UNREGISTER %s %s %d\r\n", cr.getUsername(), password, cr.getPort());
            out.flush();
            Scanner response = new Scanner(in);
            processResponse(response);
        } catch (IOException ioException) {
            String errorMessage = "Error connecting to the server while sending " +
                    "an unregister request. It isn't a problem however, because " +
                    "clients are unregistered after some time of inactivity. " +
                    "Nonetheless, please check your internet connection";
            throw new IOException(errorMessage);
        }
    }

    public ArrayList<ClientRecord> get(int n1, int n2) throws IOException {
        try (
                TTPSocket s = new TTPSocket(registryServerAddress, registryServerPort);
                BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
                PrintWriter out = new PrintWriter(new OutputStreamWriter(s.getOutputStream()), true)
        ) {
            out.format("GET %d %d\r\n", n1, n2);
            out.flush();
            Scanner response = new Scanner(in);
            processResponse(response);
            return parseRecords(response);
        } catch (IOException ioException) {
            String errorMessage = "Error fetching client records from a remote " +
                    "server. Check the correctness of provided " +
                    "ip address and/or network connectivity";
            throw new IOException(errorMessage);
        }
    }

    public ArrayList<ClientRecord> search(InetAddress ip, int port,
                                          String username) throws IOException {
        try (
                TTPSocket s = new TTPSocket(registryServerAddress, registryServerPort);
                BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
                PrintWriter out = new PrintWriter(new OutputStreamWriter(s.getOutputStream()), true)
        ) {
            StringBuilder request = new StringBuilder();
            request.append("SEARCH ");
            if (ip != null) {
                request.append(ip.getHostAddress());
                request.append(' ');
            }
            if (port != 0) {
                request.append(port);
                request.append(' ');
            }
            if (username != null) {
                request.append(username);
            }
            request.append("\r\n");

            out.write(request.toString());
            out.flush();
            Scanner response = new Scanner(in);
            processResponse(response);
            return parseRecords(response);
        } catch (IOException ioException) {
            String errorMessage = "Error fetching client records from a remote " +
                    "server. Check the correctness of provided ip address " +
                    "and/or network connectivity";
            throw new IOException(errorMessage);
        }
}

    private void processResponse(Scanner response) throws IOException, IllegalArgumentException {
        String res = response.next();
        switch (res) {
            case "OK":
                return;
            case "INVALID_REQUEST":
                throw new Error("REGISTER request malformed");
            case "SERVER_ERR":
                throw new IOException("Internal server error");
            case "PASSWD_ERR":
                throw new IllegalArgumentException("Invalid password");
            case "USER_NOT_FOUND":
                throw new Error("USER not found on the server");
            default:
                throw new IOException("Malformed server response");
        }
    }

    private ArrayList<ClientRecord> parseRecords(Scanner response) throws IOException {
        ArrayList<ClientRecord> clientRecords = new ArrayList<ClientRecord>();
        try {
            String empty = response.nextLine();
            while (response.hasNextLine()) {
                clientRecords.add(ClientRecord.parseRecord(response.nextLine()));
            }
        } catch (Exception x) {
            throw new IOException("Malformed server response");
        }
        return clientRecords;
    }
}

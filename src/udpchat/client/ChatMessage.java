package udpchat.client;

import udpchat.ClientRecord;
import udpchat.client.guiclient.Utilities;

import java.util.Date;
import java.util.Scanner;

public class ChatMessage {

    private static ClientRecord identity;
    private final String message;
    private final Date timestamp;
    private final ClientRecord sender; // Null if we are the sender

    public static void setIdentity(ClientRecord clientRecord) {
        // Called on application initialization
        identity = clientRecord;
    }

    /**
     * Constructor called by a client sending a message
     * @param message String containing the message to be sent
     */
    public ChatMessage(String message) {
        this.message = message;
        this.timestamp = new Date();
        this.sender = null;
    }

    /**
     * Constructor called by a client receiving a message.
     * @param timestamp Timestamp retrieved from a PDU
     * @param message Message string retrieved from a PDU
     * @param sender Identity of a sender retrieved from a registry server
     */
    public ChatMessage(Date timestamp, String message, ClientRecord sender) {
        this.timestamp = timestamp;
        this.sender = sender;
        this.message = message;
    }

    public boolean wasReceived() {
        return sender != null;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public String getMessage() {
        return message;
    }

    public ClientRecord getSender() {
        return sender;
    }

    public static ChatMessage parseMessage(byte[] msg, ClientRecord cr) {
        String request = new String(msg);
        Scanner s = new Scanner(request);
        String timestampString = s.next();
        String message = s.next("\r\n.*\r\n\r\n");
        Date timestamp = new Date(Long.parseLong(timestampString) * 1000);
        return new ChatMessage(timestamp, message, cr);
    }

    @Override
    public String toString() {
        return String.format("[%s] %s: %s%n",
                Utilities.formatTimestamp(timestamp),
                (sender == null) ? identity.getUsername() : sender.getUsername(),
                message);
    }
}

package udpchat.client.guiclient;

import ttp.TTPServerSocket;
import ttp.TTPSocket;
import udpchat.ClientRecord;
import udpchat.client.ChatMessage;

import javax.swing.*;
import java.io.*;
import java.net.InetAddress;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

public class MessageReceivingThread extends Thread  {
    private final MessageDatabase messageDatabase;
    private final SwingClient client;

    private AtomicBoolean shouldRun = new AtomicBoolean(true);

    public MessageReceivingThread(MessageDatabase messageDatabase, SwingClient client) {
        this.messageDatabase = messageDatabase;
        this.client = client;
    }

    @Override
    public void run() {
        int localPort = messageDatabase.getIdentity().getPort();
        while (shouldRun.get()) {
            try (
                    TTPServerSocket serverSocket = new TTPServerSocket(localPort);
                    TTPSocket socket = serverSocket.accept();
                    InputStreamReader in = new InputStreamReader(socket.getInputStream());
            ) {
                Scanner s = new Scanner(in);
                String opcode = s.next();
                int senderListenPort = Integer.parseInt(s.next());
                Date timestamp;
                try {
                    timestamp = parseTime(s.next());
                } catch (ParseException x) {
                    continue;
                }
                switch (opcode) {
                    case "MESSAGE":
                        parseMessage(s, timestamp, socket.getAddress(), senderListenPort);
                        break;
                    case "FILE":
                        parseFile(s, timestamp, socket.getAddress(), senderListenPort);
                        break;
                    default:
                        // TODO: Shouldn't there be a response? Should it be
                        //  added to the spec?
                        break;
                }
            } catch (IOException x) {
                x.printStackTrace();
            }
        }
    }

    private Date parseTime(String timestamp) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("yy/MM/dd/HH:mm:ss");
        return format.parse(timestamp);
    }

    private void parseMessage(Scanner s, Date timestamp, InetAddress senderAddress, int senderListenPort) {
        // TODO: two \r\n's may appear in user message, should other delimiter
        //  be used?
        s.useDelimiter("\r\n");
        String msg = s.next();
        SwingUtilities.invokeLater(() -> {
            ClientRecord sender = messageDatabase.findRecord(senderAddress, senderListenPort);
            ChatMessage chatMessage = new ChatMessage(timestamp, msg, sender);
            messageDatabase.addMessage(sender, chatMessage);
            ClientRecord activeContact = client.getActiveContact();
            if (activeContact != null && activeContact.equals(sender)) {
                client.synchronizeChat(sender);
            }
        });
    }

    private void parseFile(Scanner s, Date timestamp, InetAddress senderAddress, int senderListenPort) {
        // TODO: Implement, and change the GUI to suit it
    }

    public void stopThread() {
        shouldRun.set(false);
    }
}

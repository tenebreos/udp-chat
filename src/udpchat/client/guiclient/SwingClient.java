package udpchat.client.guiclient;

import udpchat.ClientRecord;
import udpchat.client.ChatMessage;
import udpchat.client.ClientConnector;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SwingClient {
    private JPanel panelMain;
    private JList<ClientRecord> listRecords;
    private JTextArea textMessage;
    private JButton buttonSend;
    private JPanel panelChats;
    private JTextField textAddress;
    private JTextField textPort;
    private JTextField textUsername;
    private JButton buttonSearch;
    private JButton buttonClear;

    private final Map<ClientRecord, JTextArea> contactsAndChats = new HashMap<>();
    private JTextArea activeChat;

    public ClientRecord getActiveContact() {
        return activeContact;
    }

    private ClientRecord activeContact;
    private final DefaultListModel<ClientRecord> listModel = new DefaultListModel<>();
    private static final ListCellRenderer<ClientRecord> CLIENT_RECORD_LIST_CELL_RENDERER =
            new ListCellRenderer<ClientRecord>() {
        @Override
        public Component getListCellRendererComponent(
                JList<? extends ClientRecord> list, ClientRecord value,
                int index, boolean isSelected, boolean cellHasFocus) {

            JPanel clientRecordPanel = new JPanel(new FlowLayout());

            JLabel username = new JLabel(value.getFullName());
            clientRecordPanel.add(username);

            String addr = value.getAddress() + ":" + value.getPort();
            JLabel ipPort = new JLabel(addr);
            ipPort.setForeground(Color.GRAY);
            clientRecordPanel.add(ipPort);

            if (isSelected) {
                // TODO: background color should be look-and-feel dependent
                clientRecordPanel.setBackground(new Color(190, 190, 190));
            }

            return clientRecordPanel;
        }
    };
    private final MessageDatabase messageDatabase;

    public SwingClient(JFrame frame, InetAddress registryAddress, int registryPort, int localPort, String username) {

        messageDatabase = new MessageDatabase(registryAddress, registryPort, localPort, username);
        ChatMessage.setIdentity(messageDatabase.getIdentity());
        listRecords.setModel(listModel);
        listRecords.setCellRenderer(CLIENT_RECORD_LIST_CELL_RENDERER);
        updateContactsList();
        MessageReceivingThread receivingThread = new MessageReceivingThread(messageDatabase, this);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                receivingThread.stopThread();
                messageDatabase.unregister();
            }
        });
        buttonSend.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                /*      SEND LISTENER       */
                String message = textMessage.getText();
                if (activeChat == null || message.equals(""))
                    return;
                sendMessage(message);
            }
        });
        textMessage.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                /*      SEND LISTENER        */
                super.keyTyped(e);
                if (e.getKeyChar() != KeyEvent.VK_ENTER)
                    return;
                String message = textMessage.getText();
                if (activeChat == null || message.equals(""))
                    return;
                sendMessage(message);
            }
        });
        listRecords.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                /*      CHAT CHANGE LISTENER      */
                ClientRecord record = listRecords.getSelectedValue();
                if (record != null) {
                    showChat(record);
                }
            }
        });
        buttonSearch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                /*      SEARCH LISTENER      */
                searchAndUpdateDatabase();
            }
        });
        buttonClear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                /*      CLEAR FIELDS LISTENER      */
                clearSearchTextfields();
            }
        });
        KeyAdapter searchFieldListener = new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                super.keyTyped(e);
                /*     SEARCH FIELD LISTENER      */
                switch (e.getKeyChar()) {
                    case KeyEvent.VK_ENTER:
                        searchAndUpdateDatabase();
                        break;
                    case KeyEvent.VK_ESCAPE:
                        clearSearchTextfields();
                        break;
                }
            }
        };
        textAddress.addKeyListener(searchFieldListener);
        textPort.addKeyListener(searchFieldListener);
        textUsername.addKeyListener(searchFieldListener);
        receivingThread.start();
    }

    private void searchAndUpdateDatabase() {
        messageDatabase.search(
                textAddress.getText(),
                textPort.getText(),
                textUsername.getText());
        updateContactsList();
    }

    private void clearSearchTextfields() {
        textAddress.setText("");
        textPort.setText("");
        textUsername.setText("");
    }

    public void showChat(ClientRecord clientRecord) {
        if (!contactsAndChats.containsKey(clientRecord)) {
            addChat(clientRecord);
        }
        synchronizeChat(clientRecord);
        CardLayout cardLayout = (CardLayout) panelChats.getLayout();
        cardLayout.show(panelChats, clientRecord.toString());
        activeChat = contactsAndChats.get(clientRecord);
        activeContact = clientRecord;
    }

    private void addChat(ClientRecord clientRecord) {
        JTextArea newChat = new JTextArea();
        newChat.setEditable(false);
        newChat.setLineWrap(true);
        newChat.setWrapStyleWord(true);
        // TODO: chat JTextFields in the UI don't fit text to the window size
        //  properly.
        // newChat.setColumns(20);
        contactsAndChats.put(clientRecord, newChat);
        panelChats.add(newChat, clientRecord.toString());
    }

    private void sendMessage(String message) {
        try {
            // TODO: should only delete the last '\n'
            ChatMessage chatMessage = new ChatMessage(message.replaceAll("\n", ""));
            ClientConnector.sendMessage(messageDatabase, activeContact, chatMessage);
            messageDatabase.addMessage(activeContact, chatMessage);
            textMessage.setText("");
            activeChat.setText(activeChat.getText() + chatMessage);
        } catch (IOException ioException) {
            JOptionPane.showMessageDialog(panelMain,
                    "Error sending message!", "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    private void updateContactsList() {
        listModel.removeAllElements();
        for (ClientRecord record : messageDatabase.getRecords()) {
            if (!record.equals(messageDatabase.getIdentity())) {
                listModel.addElement(record);
            }
        }
    }

    public void synchronizeChat(ClientRecord cr) {
        // TODO: remember that timestamps in messages are in UTC, convert them
        //  to local time before displaying.
        ArrayList<ChatMessage> messages = messageDatabase.getMessages(cr);
        JTextArea chat = contactsAndChats.get(cr);
        chat.setText("");
        for (ChatMessage message : messages) {
            chat.setText(chat.getText() + message);
        }
    }

    public static void main(String[] args) throws Exception {
        if (args.length < 4) {
            System.out.println("Usage: SwingClient <registryAddress> <registryPort> <localPort> <username>");
            return;
        }
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        InetAddress registryAddress = InetAddress.getByName(args[0]);
        int registryPort = Integer.parseInt(args[1]);
        int localPort = Integer.parseInt(args[2]);
        String username = args[3];
        JFrame frame = new JFrame("UDP-Chat");
        SwingClient client = new SwingClient(frame, registryAddress, registryPort, localPort, username);
        frame.setContentPane(client.panelMain);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}

package udpchat.client.guiclient;

import udpchat.ClientRecord;
import udpchat.client.ChatMessage;
import udpchat.client.RegistryServerConnector;

import javax.swing.*;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class MessageDatabase {

    private final ClientRecord identity;
    private final Map<ClientRecord, ArrayList<ChatMessage>> database = new HashMap<>();
    private final RegistryServerConnector registryConnector;
    private final long DEFAULT_TTL = 300;//s //TODO: perhaps specify this in config options?
    private final String registryPassword;

    public MessageDatabase(InetAddress registryAddress, int registryPort, int localPort, String username) {
        identity = new ClientRecord(Utilities.getLocalAddress(), localPort, username, DEFAULT_TTL);
        registryConnector = new RegistryServerConnector(registryAddress, registryPort);
        registryPassword = generateNewPassword();
        Timer registrationTimer = new Timer((int) DEFAULT_TTL - 5, e -> {
            try {
                registryConnector.register(identity, registryPassword);
            } catch (IOException x) {
                JOptionPane.showMessageDialog(null,
                        "Network error while connecting to registry," +
                                "Please check your connectivity.");
            }
        });
        try {
            registryConnector.register(identity, registryPassword);
            ArrayList<ClientRecord> records = registryConnector.get(0, 2);
            for (ClientRecord record : records) {
                database.put(record, new ArrayList<>());
            }
        } catch (IOException ioException) {
            String errorMessage = ioException.getMessage();
            JOptionPane.showMessageDialog(null, errorMessage,
                    "Registry Connection Error", JOptionPane.ERROR_MESSAGE);
            System.exit(2);
        }
    }

    private static String generateNewPassword() {
        // TODO: generates passwords consisting of numbers only. FIX!
        StringBuilder password = new StringBuilder();
        Random random = new Random();
        int i = 20;
        while (i-- > 0) {
            char c;
            int res1 = random.nextInt(3);
            switch (res1) {
                case 0:
                    c = (char) (random.nextInt(10) + '0');
                    break;
                case 1:
                    c = (char) (random.nextInt(26) + 'a');
                    break;
                case 2:
                    c = (char) (random.nextInt(26) + 'A');
                    break;
            }
            password.append(res1);
        }
        return password.toString();
    }

    public void search(InetAddress address, int port, String username) {
        try {
            ArrayList<ClientRecord> records = registryConnector.search(address, port, username);
            for (ClientRecord record : records) {
                if (!database.containsKey(record)) {
                    database.put(record, new ArrayList<>());
                }
            }
        } catch (UnknownHostException x) {
            x.printStackTrace();
        } catch (IOException ioException) {
            JOptionPane.showMessageDialog(null,
                    ioException.getMessage(), "Network error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    public void search(String address, String port, String username) {
        boolean addressEmpty = address.equals(""),
                portEmpty = port.equals(""),
                usernameEmpty = username.equals("");
        if (addressEmpty && portEmpty && usernameEmpty) {
            return;
        }
        InetAddress ip = null;
        int _port = 0;
        if (!addressEmpty) {
            try {
                ip = InetAddress.getByName(address);
            } catch (UnknownHostException unknownHostException) {
                return;
            }
        }
        if (!portEmpty) {
            _port = Integer.parseInt(port);
        }
        if (usernameEmpty) {
            username = null;
        }
        search(ip, _port, username);
    }

    public ClientRecord findRecord(InetAddress ip, int port) {
        for (ClientRecord record : database.keySet()) {
            if (record.getAddress().equals(ip) && record.getPort() == port) {
                return record;
            }
        }

        search(ip, port, null);

        // TODO: Searches the whole database two times, replace with something
        //  which scales better.
        for (ClientRecord record : database.keySet()) {
            if (record.getAddress().equals(ip) && record.getPort() == port) {
                return record;
            }
        }

        // TODO: Should non-expiring records be possible?
        ClientRecord unknownUserRecord = new ClientRecord(ip, port, "Unknown", 600);
        database.put(unknownUserRecord, new ArrayList<>());
        return unknownUserRecord;
    }

    public void unregister() {
        try {
            registryConnector.unregister(identity, registryPassword);
        } catch (Exception x) {
            JOptionPane.showMessageDialog(null,
                    x.getMessage(), "Error", JOptionPane.ERROR_MESSAGE );
        }
    }

    public ClientRecord getIdentity () {
        return identity;
    }

    public ArrayList<ChatMessage> getMessages (ClientRecord clientRecord){
        return database.get(clientRecord);
    }

    public ArrayList<ClientRecord> getRecords () {
        return new ArrayList<>(database.keySet());
    }

    public void addMessage (ClientRecord activeContact,
                            ChatMessage chatMessage) {
        ArrayList<ChatMessage> messages = database.get(activeContact);
        messages.add(chatMessage);
    }
}

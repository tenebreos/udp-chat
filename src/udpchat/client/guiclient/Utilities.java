package udpchat.client.guiclient;

import javax.swing.*;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.SimpleTimeZone;

public class Utilities {

    public static String formatTimestamp(Date d) {
        return DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(d);
    }

    public static String formatTime(Date d) {
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("yy/MM/dd/HH:mm:ss");
        return format.format(d);
    }

    private static InetAddress localAddress;
    private static InetAddress findLocalAddress() {
        InetAddress localAddress = null;
        try {
            Enumeration<NetworkInterface> interfaces =
                    NetworkInterface.getNetworkInterfaces();
            for (NetworkInterface networkInterface : Collections.list(interfaces)) {
                if (networkInterface.isUp()
                        && !networkInterface.isLoopback()
                        && !networkInterface.isVirtual()
                        && !networkInterface.isPointToPoint()) {
                    Enumeration<InetAddress> addresses = networkInterface.getInetAddresses();
                    for (InetAddress address : Collections.list(addresses)) {
                        if (address instanceof Inet4Address) {
                            localAddress = address;
                        }
                    }
                }
            }
        } catch (SocketException x) {
            errorWhileGettingLocalAddress();
        }
        if (localAddress == null) {
            errorWhileGettingLocalAddress();
        }
        return localAddress;
    }

    private static void errorWhileGettingLocalAddress() {
        JOptionPane.showMessageDialog(null,
                "Error getting local ip", "Error", JOptionPane.ERROR_MESSAGE);
        System.exit(1);
    }

    public static InetAddress getLocalAddress() {
        if (localAddress == null) {
            localAddress = findLocalAddress();
            if (localAddress == null)
                throw new NullPointerException();
        }
        return localAddress;
    }
}

package udpchat.client;

import ttp.TTPSocket;
import udpchat.ClientRecord;
import udpchat.client.guiclient.MessageDatabase;
import udpchat.client.guiclient.Utilities;

import java.io.*;
import java.util.Base64;
import java.util.Date;

/**
 * Encapsulates protocol methods for communicating with other clients
 */
public class ClientConnector {

    public static void sendMessage(MessageDatabase database, ClientRecord cr, String message) throws IOException {
        ChatMessage msg = new ChatMessage(message);
        sendMessage(database, cr, msg);
    }

    public static void sendMessage(MessageDatabase database, ClientRecord cr, ChatMessage message) throws IOException {
        try (
                TTPSocket s = new TTPSocket(cr.getAddress(), cr.getPort());
                PrintWriter out = new PrintWriter(new OutputStreamWriter(s.getOutputStream()))
        ) {
            out.format("MESSAGE %d %s\r\n%s\r\n\r\n",
                    database.getIdentity().getPort(),
                    Utilities.formatTime(message.getTimestamp()),
                    message.getMessage());
            out.flush();
        }
    }

    public static void sendFile(ClientRecord cr, String filepath) throws IOException {
        try (
                TTPSocket s = new TTPSocket(cr.getAddress(), cr.getPort());
                PrintWriter out = new PrintWriter(new OutputStreamWriter(s.getOutputStream()));
                FileInputStream fin = new FileInputStream(filepath);
        ) {
            Base64.Encoder b64enc = Base64.getEncoder();
            String b64file = b64enc.encodeToString(fin.readAllBytes());
            out.format("FILE %s\r\n", (new Date().getTime() / 1000));
            out.write(b64file);
            out.print("\r\n\r\n");
            out.flush();
        }
    }
}

package udpchat.server;

import static udpchat.GlobalRegexes.*;

import ttp.TTPSocket;
import udpchat.ClientRecord;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.regex.Pattern;

public class RegistryServerThread extends Thread {

    private static final String response_ok = "OK\r\n";
    private static final String response_invalid_request = "INVALID_REQUEST\r\n";
    private static final String response_user_not_found = "USER_NOT_FOUND\r\n";
    private static final String response_password_error = "PASSWD_ERR\r\n";

    private final ConcurrentLinkedDeque<RegistryRecord> recordList;
    private final PrintWriter out;
    private final BufferedReader in;
    private final TTPSocket socket;

    public RegistryServerThread(TTPSocket s,
                                ConcurrentLinkedDeque<RegistryRecord> recordList) {
        this.recordList = recordList;
        this.socket = s;
        this.out = new PrintWriter(new OutputStreamWriter(s.getOutputStream()));
        this.in = new BufferedReader(new InputStreamReader(s.getInputStream()));
    }

    private final String logFormat = "[%s] %s request from %s%n";
    @Override
    public void run() {
        try {
            Scanner s = new Scanner(in);
            String opcode = s.next();
            System.out.format(logFormat, timestamp(), opcode, socket.getAddress());
            switch (opcode) {
                case "REGISTER": {
                    String username = s.next();
                    int port = s.nextInt();
                    String password = s.next();
                    long ttl = s.nextLong();
                    requestRegister(username, port,  ttl, password);
                } break;
                case "UNREGISTER": {
                    String username = s.next();
                    String password = s.next();
                    int port = s.nextInt();
                    requestUnregister(username, password, port);
                } break;
                case "GET": {
                    int n1 = s.nextInt();
                    int n2 = s.nextInt();
                    requestGet(n1, n2);
                } break;
                case "SEARCH": {
                    s.useDelimiter("\r\n");
                    String requestString = s.next();
                    processSearchRequest(requestString);
                } break;
                default:
                    responseInvalidRequest();
            }
        } catch (NoSuchElementException x) {
            responseInvalidRequest();
        } finally {
            socket.close();
        }
    }

    private void processSearchRequest(String requestString) {
        InetAddress ip = null;
        String username = null;
        int port = 0;

        Scanner searchStringScanner = new Scanner(requestString);
        final Integer IP = 0;
        final Integer PORT = 1;
        final Integer NAME = 2;
        ArrayList<Integer> possible_tokens = new ArrayList<>(Arrays.asList(IP, PORT, NAME));
        while (searchStringScanner.hasNext()) {
            if (searchStringScanner.hasNext(ip_regex)) {
                if (!possible_tokens.contains(IP)) {
                    responseInvalidRequest();
                    return;
                }
                try {
                    ip = InetAddress.getByName(searchStringScanner.next(ip_regex));
                } catch (UnknownHostException unknownHostException) {
                    unknownHostException.printStackTrace();
                }
                possible_tokens.remove(IP);
            } else if (searchStringScanner.hasNext(port_regex)) {
                if (!possible_tokens.contains(PORT)) {
                    responseInvalidRequest();
                    return;
                }
                port = Integer.parseInt(searchStringScanner.next(port_regex));
                possible_tokens.removeAll(Arrays.asList(IP, PORT));
            } else if (searchStringScanner.hasNext(name_regex)) {
                if (!possible_tokens.contains(NAME)) {
                    responseInvalidRequest();
                    return;
                }
                username = searchStringScanner.next(name_regex);
                possible_tokens.removeAll(Arrays.asList(IP, PORT, NAME));
            } else {
                responseInvalidRequest();
            }
        }
        requestSearch(ip, port, username);
    }

    private void responseInvalidRequest() {
        out.print(response_invalid_request);
        out.flush();
    }

    private void requestSearch(InetAddress addr, int port, String username) {
        ArrayList<ClientRecord> found = new ArrayList<>();
        for (RegistryRecord rr : recordList) {
            ClientRecord r = rr.getRecord();
            boolean addressMatches = addr == null || r.getAddress().equals(addr);
            boolean portMatches = port == 0 || port == r.getPort();
            boolean usernameMatches = username == null || r.getUsername().equals(username);
            if (addressMatches && portMatches && usernameMatches)
                found.add(r);
        }

        StringBuilder response = new StringBuilder();
        response.append(response_ok);
        for (ClientRecord r : found) {
            response.append(r);
            response.append("\r\n");
        }
        out.print(response.toString());
        out.flush();
    }

    private void requestGet(int n1, int n2) {
        ArrayList<ClientRecord> recs = new ArrayList<>();
        int idx = 0;
        for (RegistryRecord rr : recordList) {
            if (idx >= n1 && idx < n2)
                recs.add(rr.getRecord());
            idx++;
        }

        StringBuilder response = new StringBuilder();
        response.append(response_ok);
        for (ClientRecord r : recs) {
            response.append(r);
            response.append("\r\n");
        }
        out.print(response.toString());
        out.flush();
    }

    private void requestUnregister(String username, String password, int port) {
        ClientRecord cr = new ClientRecord(socket.getAddress(), port, username, 0);
        RegistryRecord rr = new RegistryRecord(cr, password);
        boolean recordFound = false, recordDeleted = false;
        for (RegistryRecord record : recordList) {
            if (record.equals(rr)) {
                recordFound = true;
                if (record.getPassword().equals(password)) {
                    recordList.remove(record);
                    recordDeleted = true;
                }
            }
        }
        if (recordFound && recordDeleted) {
            out.print(response_ok);
            out.flush();
        } else if (recordFound) {
            out.print(response_password_error);
            out.flush();
        } else {
            out.print(response_user_not_found);
            out.flush();
        }
        RegistryServer.listRegistryContents(recordList);
    }

    private void requestRegister(String username, int p2pPort, long ttl, String password) {
        ClientRecord cr = new ClientRecord(socket.getAddress(), p2pPort, username, ttl);
        RegistryRecord rr = new RegistryRecord(cr, password);
        if (!recordList.contains(rr))
            recordList.add(rr);
        out.print(response_ok);
        out.flush();
        RegistryServer.listRegistryContents(recordList);
    }

    private static String timestamp() {
        return new Date().toString();
    }
}

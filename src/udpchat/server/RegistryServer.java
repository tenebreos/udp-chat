package udpchat.server;

import ttp.TTPServerSocket;
import ttp.TTPSocket;
import udpchat.ClientRecord;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.concurrent.ConcurrentLinkedDeque;


public class RegistryServer {

    private final ConcurrentLinkedDeque<RegistryRecord> recordList = new ConcurrentLinkedDeque<>();

    public ConcurrentLinkedDeque<RegistryRecord> getRecordList() {
        return recordList;
    }

    public static void main(String[] args) throws Exception {
        if (args.length < 1) {
            System.err.println("Usage: java RegistryServer <local_port>");
            return;
        }
        int local_port = Integer.parseInt(args[0]);
        RegistryServer server = new RegistryServer();
//        server.putMockRecordsInRegistry();
        try ( TTPServerSocket ss = new TTPServerSocket(local_port) ) {
            while (true) {
                TTPSocket s = ss.accept();
                new RegistryServerThread(s, server.getRecordList()).start();
            }
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    public static void listRegistryContents(ConcurrentLinkedDeque<RegistryRecord> recordList) {
        for (RegistryRecord record : recordList) {
            ClientRecord clientRecord = record.getRecord();
            System.out.format("- %s\t%s:%d\t%s%n", clientRecord.getFullName(),
                    clientRecord.getAddress(), clientRecord.getPort(),
                    record.getPassword());
        }
    }

    private void putMockRecordsInRegistry() {
        for (ClientRecord record : getMockRecords()) {
            recordList.add(new RegistryRecord(record, "asdf"));
        }
        listRegistryContents(recordList);
    }

    private static ArrayList<ClientRecord> getMockRecords() {
        ArrayList<ClientRecord> records = null;
        records = new ArrayList<>();
        try {
            records.add(new ClientRecord(InetAddress.getByName("192.168.0.3"), 2137, "Arnold", 600));
            records.add(new ClientRecord(InetAddress.getByName("192.168.0.16"), 1337, "Avicavrof", 600));
            records.add(new ClientRecord(InetAddress.getByName("192.168.0.5"), 13000, "kot", 600));
            records.add(new ClientRecord(InetAddress.getByName("192.168.0.17"), 14000, "Gaia", 600));
            records.add(new ClientRecord(InetAddress.getByName("192.168.0.24"), 15000, "Flip", 600));
            records.add(new ClientRecord(InetAddress.getByName("192.168.0.25"), 15000, "Flop", 600));
            records.add(new ClientRecord(InetAddress.getByName("192.168.0.40"), 17000, "Dog", 600));
            records.add(new ClientRecord(InetAddress.getByName("192.168.0.41"), 17000, "Dog", 600));
            records.add(new ClientRecord(InetAddress.getByName("192.168.0.42"), 17000, "Dog", 600));
            records.add(new ClientRecord(InetAddress.getByName("192.168.0.43"), 17000, "Dog", 600));
        } catch (UnknownHostException x) {
            x.printStackTrace();
        }
        return records;
    }
}

package udpchat.server;

import udpchat.ClientRecord;

import java.util.Objects;

public class RegistryRecord {

    private final ClientRecord record;
    private final String password;

    public ClientRecord getRecord() {
        return record;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegistryRecord that = (RegistryRecord) o;
        return record.equals(that.record);
    }

    @Override
    public int hashCode() {
        return Objects.hash(record);
    }

    public RegistryRecord(ClientRecord record, String password) {
        this.record = record;
        this.password = password;
    }
}

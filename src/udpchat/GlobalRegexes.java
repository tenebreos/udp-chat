package udpchat;

import java.util.regex.Pattern;

public class GlobalRegexes {
    public static final Pattern ip_regex = Pattern.compile("([\\d]{1,3}\\.){3}([\\d]{1,3}){1}");
    public static final Pattern name_regex = Pattern.compile("[\\p{Alnum}\\p{Punct}]+");
    public static final Pattern port_regex = Pattern.compile("[\\d]{1,5}");
}

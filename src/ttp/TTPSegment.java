package ttp;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.nio.ByteBuffer;

/**
 * Represents a TTP segment
 */
public class TTPSegment {

    /* Provided by UDP:
        - source port
        - destination port
        - checksum
        - payload length */

    /*  Segment scheme:
        1      8      16      24      32
        |------|-------|-------|-------|
        |        Sequence number       |
        |------|-------|-------|-------|
        |    Acknowledgement number    |
        |------|-------|-------|-------|
        | Flags|                       |
        |------|                       |
        |             Data             |
        |                              |
        |                              |
        |------|-------|-------|-------|
        All stored in big endian.
     */

    public static final byte ACK = 1;
    public static final byte SYN = 2;
    public static final byte FIN = 4;
    public static final int HEADER_LENGTH = 9;
    // TODO: It is possible to match the MSS to the MTU of a machine via the NetworkInterface::getMTU() method.
    public static final int MSS = 1000; // not including the header, number chosen at random

    private int seqnum;
    private int acknum;
    private byte flags;
    private byte[] data;

    public TTPSegment(int seqnum, int acknum, byte flags, byte[] data) {
        this.seqnum = seqnum;
        this.acknum = acknum;
        this.flags  = flags;
        this.data   = data;
    }

    /**
     * Static methods allowing for unpacking the segment from a datagram
     * @param packet UDP datagram containing the segment
     * @return TTPSegment unpacked from the datagram
     */
    static TTPSegment extract(DatagramPacket packet) {
        ByteBuffer encodedSegment = ByteBuffer.wrap(packet.getData());
        int seqNum = encodedSegment.getInt();
        int ackNum = encodedSegment.getInt();
        byte flags  = encodedSegment.get();
        int dataLength = packet.getLength() - TTPSegment.HEADER_LENGTH;
        byte[] data = null;
        if (dataLength > 0) {
            data = new byte[dataLength];
            encodedSegment.get(data, 0, dataLength);
        }
        return new TTPSegment(seqNum, ackNum, flags, data);
    }

    /**
     * A method allowing for encapsulating a TTPSegment into a datagram
     * @param dstAddress address to which this datagram should be delivered
     * @param dstPort port to which this datagram should be delivered
     * @return
     */
    protected DatagramPacket encapsulate(InetAddress dstAddress, int dstPort) {
        byte[] segment = encode();
        return new DatagramPacket(segment, segment.length, dstAddress, dstPort);
    }

    /**
     * Used to serialize a TTPSegment for transmission on the network
     * @return
     */
    private byte[] encode() {
        int dataLength = (data != null) ? data.length : 0;
        // ByteBuffers are big endian by default
        ByteBuffer packet = ByteBuffer.allocate(HEADER_LENGTH + dataLength);
        packet.putInt(seqnum);
        packet.putInt(acknum);
        packet.put(flags);
        if (data != null)
            packet.put(data);
        return packet.array();
    }

    public byte[] getData() {
        return data;
    }

    public int getAcknum() {
        return acknum;
    }

    public int getSeqnum(){
        return seqnum;
    }

    public boolean isSYN() {
        return (flags & TTPSegment.SYN) != 0;
    }

    public boolean isACK() {
        return (flags & TTPSegment.ACK) != 0;
    }

    public boolean isFIN() {
        return (flags & TTPSegment.FIN) != 0;
    }

    public boolean hasData() {
        return data != null;
    }

}

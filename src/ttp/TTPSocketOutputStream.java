package ttp;

import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class TTPSocketOutputStream extends OutputStream {

    private final TTPSocket socket;

    public TTPSocketOutputStream(TTPSocket socket) {
        this.socket = socket;
    }

    @Override
    public void write(byte[] b) {
        write(b, 0, b.length);
    }

    @Override
    public void write(byte[] b, int off, int len) {
        byte[] data = Arrays.copyOfRange(b, off, len);
        socket.queueForTransmission(data);
    }

    @Override
    public void write(int b) {
        byte[] data = ByteBuffer.allocate(4).putInt(b).array();
        write(data);
    }
}

package ttp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class TTPSocket implements AutoCloseable, Runnable {
    /**
     * The amount of time the dispatcher should wait for an arrival of a packet.
     * If a packet has arrived beforehand, it was be cached by the OS and will
     * be delivered immediately.
     */
    private final static int TIMEOUT = 50;
    private final static long TEARDOWN_TIMEOUT = 30000;  // ms
    public enum ConnectionState { UNHANDSHAKEN, HANDSHAKE_IN_PROGRESS, ERROR, CONNECTED, HALF_SHUT, SHUTDOWN }
    private enum SocketType { SERVER_CONNECTION_SOCKET, CLIENT_SOCKET }

    private ConnectionState connectionState; // Multi threaded access
    private AtomicBoolean calledForShutdown = new AtomicBoolean(false);

    private final SocketType socketType;
    private TTPServerSender serverSender;
    private TTPServerReceiver serverReceiver;
    private TTPClientSender clientSender;
    private TTPClientReceiver clientReceiver;

    private final DatagramSocket socket;
    private final InetAddress destinationAddress;
    private int destinationPort;

    private long endTime = 0;
    private boolean timerRunning = false;
    private long teardownEndTime = 0;
    private boolean teardownTimerRunning = false;

    private final BlockingQueue<byte[]> sendQueue = new LinkedBlockingQueue<>();
    private final BlockingQueue<byte[]> receiveQueue = new LinkedBlockingQueue<>();
    // TODO: implement a bufferQueue


    /**
     * Constructor used by the TTPServerSocket to create a TTPSocket accepting a
     * handshake.
     * @param destinationAddress port of socket to exchange data with.
     * @param destinationPort address of host to exchange data with.
     * @param initialSeqNum initial sequence number, to initialize TTPServerSender
     * @param expectedSeqNum initial acknowledge number, to initialize TTPServerReceiver with
     */
    protected TTPSocket(TTPServerSocket serverSocket,
                        InetAddress destinationAddress, int destinationPort,
                        int initialSeqNum, int expectedSeqNum) throws IOException {

        this.connectionState = ConnectionState.HANDSHAKE_IN_PROGRESS;
        this.socketType = SocketType.SERVER_CONNECTION_SOCKET;
        this.destinationAddress = destinationAddress;
        this.destinationPort = destinationPort;
        socket = new DatagramSocket();
        socket.connect(destinationAddress, destinationPort);
        socket.setSoTimeout(TIMEOUT);
        serverSender = new TTPServerSender(initialSeqNum, destinationPort,
                destinationAddress, socket, this, serverSocket);
        serverReceiver = new TTPServerReceiver(this, socket,
                destinationAddress, destinationPort, expectedSeqNum);
        serverSender.setReceiver(serverReceiver);
        serverReceiver.setSender(serverSender);

        String threadName = "TTPSocket-Server-" + socket.getLocalPort();
        new Thread(this, threadName).start();
    }

    /**
     * Constructor used by an application to connect to a TTPServerSocket.
     * @param destinationAddress address of the destination TTPServerSocket
     * @param destinationPort port of the destination TTPServerSocket
     * @param localPort local port number to bind the TTPSocket to
     */
    public TTPSocket(InetAddress destinationAddress, int destinationPort,
                     int localPort) throws SocketException, PortUnreachableException {

        this.connectionState = ConnectionState.UNHANDSHAKEN;
        socketType = SocketType.CLIENT_SOCKET;
        this.destinationAddress = destinationAddress;
        this.destinationPort = destinationPort;

        if (localPort == 0)
            socket = new DatagramSocket();
        else
            socket = new DatagramSocket(localPort);
        socket.setSoTimeout(TIMEOUT);
        socket.connect(destinationAddress, destinationPort);

        clientSender = new TTPClientSender(makeInitialSeqnum(), destinationPort,
                destinationAddress, socket, this);
        clientReceiver = new TTPClientReceiver(this, socket,
                destinationAddress, destinationPort);
        clientSender.setReceiver(clientReceiver);
        clientReceiver.setSender(clientSender);

        String threadName = "TTPSocket-Client-" + socket.getLocalPort();
        new Thread(this, threadName).start();
        synchronized (this) {
            while (
                    this.connectionState != ConnectionState.CONNECTED &&
                    this.connectionState != ConnectionState.ERROR
            ) {
                try {
                    wait();
                } catch (InterruptedException x) {}
            }
            if (this.connectionState == ConnectionState.ERROR)
                throw new PortUnreachableException();
        }
    }

    public TTPSocket(InetAddress destinationAddress, int destinationPort) throws SocketException {
        this(destinationAddress, destinationPort, 0);
    }

    public TTPSocket(String hostname, int destinationPort) throws SocketException, UnknownHostException {
        this(InetAddress.getByName(hostname), destinationPort);
    }

    /**
     * Dispatching loop of the socket
     */
    @Override
    public void run() {
        if (socketType == SocketType.CLIENT_SOCKET) {
            clientDispatcherLoop();
        } else if (socketType == SocketType.SERVER_CONNECTION_SOCKET) {
            serverDispatcherLoop();
        }
    }

    private void clientDispatcherLoop() {
        try {
            clientSender.handshake();
            byte[] buffer = new byte[1024];
            DatagramPacket rcvpkt = new DatagramPacket(buffer, buffer.length);
            while (true) {

                if (timerRunning && System.currentTimeMillis() >= endTime) {
                    timerRunning = false;
                    clientSender.timeout();
                }

                if (teardownTimerRunning && System.currentTimeMillis() >= teardownEndTime) {
                    teardownTimerRunning = false;
                    clientReceiver.timeout();
                }

                if (connectionState == ConnectionState.SHUTDOWN) {
                    assert clientSender.state == ttp.TTPClientSender.State.SHUTDOWN
                            && clientReceiver.state == ttp.TTPClientReceiver.State.SHUTDOWN;
                    socket.close();
                    return;
                }

                if (calledForShutdown.get()
                        && connectionState == ConnectionState.CONNECTED
                        && sendQueue.isEmpty()
                        && !clientSender.hasUnackedSegs()) {
                    connectionState = ConnectionState.HALF_SHUT;
                    clientSender.close();
                }

                if (clientSender.isShut() && clientReceiver.isShut()) {
                    connectionState = ConnectionState.SHUTDOWN;
                }

                try {
                    socket.receive(rcvpkt);
                    if (!clientSender.isShut())
                        clientSender.callFromBelow(rcvpkt);
                    if (!clientReceiver.isShut())
                        clientReceiver.callFromBelow(rcvpkt);
                } catch (SocketTimeoutException x) {
                }

                if (!sendQueue.isEmpty()
                            && connectionState == ConnectionState.CONNECTED) {
                    try {
                        byte[] data = sendQueue.take();
                        clientSender.callFromAbove(data);
                    } catch (InterruptedException x) {
                    }
                }
            }
        } catch (PortUnreachableException x) {
            synchronized (this) {
                connectionState = ConnectionState.ERROR;
                notifyAll();
            }
        } catch (IOException x) {
            connectionState = ConnectionState.ERROR;
            x.printStackTrace();
        }
    }

    private void serverDispatcherLoop() {
        try {
            byte[] buffer = new byte[1024];
            DatagramPacket rcvpkt = new DatagramPacket(buffer, buffer.length);
            while (true) {

                if (timerRunning && System.currentTimeMillis() >= endTime) {
                    timerRunning = false;
                    serverSender.timeout();
                }

                if (teardownTimerRunning && System.currentTimeMillis() >= teardownEndTime) {
                    teardownTimerRunning = false;
                    serverReceiver.timeout();
                }

                if (connectionState == ConnectionState.SHUTDOWN) {
                    assert serverSender.state == ttp.TTPServerSender.State.SHUTDOWN
                            && serverReceiver.state == ttp.TTPServerReceiver.State.SHUTDOWN;
                    socket.close();
                    return;
                }

                if (calledForShutdown.get()
                        && connectionState == ConnectionState.CONNECTED
                        && sendQueue.isEmpty()
                        && !serverSender.hasUnackedSegs()) {
                    connectionState = ConnectionState.HALF_SHUT;
                    serverSender.close();
                }

                if (serverSender.isShut() && serverReceiver.isShut()) {
                    connectionState = ConnectionState.SHUTDOWN;
                }

                try {
                    socket.receive(rcvpkt);
                    if (!serverSender.isShut())
                        serverSender.callFromBelow(rcvpkt);
                    if (!serverReceiver.isShut())
                        serverReceiver.callFromBelow(rcvpkt);
                } catch (SocketTimeoutException x) {
                }

                if (!sendQueue.isEmpty()
                        && connectionState == ConnectionState.CONNECTED) {
                    try {
                        byte[] data = sendQueue.take();
                        serverSender.callFromAbove(data);
                    } catch (InterruptedException x) {
                    }
                }
            }
        } catch (SocketTimeoutException x) {
            connectionState = ConnectionState.ERROR;
        } catch (IOException x) {
            connectionState = ConnectionState.ERROR;
            x.printStackTrace();
        }
    }

    protected void queueForTransmission(byte[] data) {
        ArrayList<byte[]> msgs = packData(data);
        for (byte[] msg: msgs) {
            while (true) {
                try {
                    sendQueue.put(msg);
                    break;
                } catch (InterruptedException x) {
                }
            }
        }
    }

    protected boolean isMessageAvailable() {
        return !receiveQueue.isEmpty();
    }

    protected byte[] getLatestMessage() {
        while (true) {
            try {
                return receiveQueue.take();
            } catch (InterruptedException interruptedException) {}
        }
    }

    protected void deliverData(byte[] data) {
        while (true) {
            try {
                receiveQueue.put(data);
                break;
            } catch (InterruptedException x) {}
        }
    }

    private static ArrayList<byte[]> packData(byte[] b) {
        int n_segs = b.length / TTPSegment.MSS + 1;
        ArrayList<byte[]> msgs = new ArrayList<>();
        int last_idx = 0, current_idx = 0;
        for (int i = 0; i < n_segs; i++) {
            last_idx = current_idx;
            current_idx = Math.min(current_idx + TTPSegment.MSS, b.length);
            msgs.add(Arrays.copyOfRange(b, last_idx, current_idx));
        }
        return msgs;
    }

    protected static int makeInitialSeqnum() {
        return (int) (Math.random() * 0xFFFFFFFF);
    }

    @Override
    public synchronized void close() {
        if (connectionState == ConnectionState.CONNECTED)
            calledForShutdown.set(true);
    }

    public void startTimer(long timeout) {
        if (timerRunning)
            throw new IllegalStateException();

        timerRunning = true;
        endTime = System.currentTimeMillis() + timeout;
    }

    public void stopTimer() {
        timerRunning = false;
    }

    public void startTeardownTimer() {
        if (teardownTimerRunning)
            throw new IllegalStateException();

        teardownTimerRunning = true;
        teardownEndTime = System.currentTimeMillis() + TEARDOWN_TIMEOUT;
    }

    public boolean isReceiverShut() {
        return (clientReceiver != null && !clientReceiver.isWaiting()) ||
               (serverReceiver != null && !serverReceiver.isWaiting());
    }

    protected void setConnectionState(ConnectionState state) {
        this.connectionState = state;
    }
    public int getDestinationPort() {
        return this.destinationPort;
    }

    public int getLocalPort() {
        return this.socket.getLocalPort();
    }

    public InetAddress getAddress() {
        return this.destinationAddress;
    }

    public InputStream getInputStream() {
        return new TTPSocketInputStream(this);
    }

    public OutputStream getOutputStream() {
        return new TTPSocketOutputStream(this);
    }
}

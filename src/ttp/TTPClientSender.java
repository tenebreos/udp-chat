package ttp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayDeque;

public class TTPClientSender implements AutoCloseable {

    public boolean isWaiting() {
        return state == State.WAIT;
    }

    protected enum State {
        NOT_INITIALISED,
        WAIT_FOR_SYNACK,
        WAIT,
        WAIT_FOR_ACK,
        WAIT_FOR_FIN,
        SHUTDOWN
    }
    private int targetPort;
    private final InetAddress targetAddress;
    private final DatagramSocket out;

    private final TTPSocket dispatcherLoop;
    private TTPClientReceiver clientReceiver;

    private int nextseqnum;
    private int base;
    private int finseqnum;
    private final int WINDOW_SIZE = 10;
    private final ArrayDeque<DatagramPacket> unackedQ = new ArrayDeque<>();
    private final ArrayDeque<DatagramPacket> tosendQ = new ArrayDeque<>();

    protected State state = State.NOT_INITIALISED;
    private boolean sentFIN = false;

    private long estimatedRTT = 1000;  // ms
    private long estimatedDEV = 0;  // ms
    private long estimatedTimeout  = 1000;  // ms
    // TODO: define RTT calculation

    public TTPClientSender(int nextseqnum, int server_port,
                           InetAddress server_address, DatagramSocket out,
                           TTPSocket dispatcherLoop) {
        this.targetPort = server_port;
        this.targetAddress = server_address;
        this.out = out;

        this.dispatcherLoop = dispatcherLoop;

        this.nextseqnum = nextseqnum;
        this.base = nextseqnum;
    }

    public void setReceiver(TTPClientReceiver clientReceiver) {
        this.clientReceiver = clientReceiver;
    }

    public void setTargetPort(int targetPort) {
        this.targetPort = targetPort;
    }

    public void handshake() throws IOException {
        if (state != State.NOT_INITIALISED)
            throw new IllegalStateException("TTPClientSender::handshake may only be called when TTPClientSender.state == UNHANDSHAKEN");

        dispatcherLoop.setConnectionState(TTPSocket.ConnectionState.HANDSHAKE_IN_PROGRESS);
        DatagramPacket sndpkt = new TTPSegment(nextseqnum, 0, TTPSegment.SYN, null)
                .encapsulate(targetAddress, targetPort);
        nextseqnum++;
        dispatcherLoop.startTimer(estimatedTimeout);
        out.send(sndpkt);
        unackedQ.offer(sndpkt);
        state = State.WAIT_FOR_SYNACK;
    }

    public void callFromAbove(byte[] data) throws IOException {
        if (state != State.WAIT)
            throw new IllegalStateException("TTPClientSender may only accept data when properly running.");

        DatagramPacket sndpkt = new TTPSegment(nextseqnum, 0, (byte) 0, data)
                .encapsulate(targetAddress, targetPort);
        send(sndpkt);
    }

    public void callFromBelow(DatagramPacket rcvpkt) {
        if (state == State.NOT_INITIALISED || state == State.SHUTDOWN)
            throw new IllegalStateException("TTPClientSender may only receive data after a handshake or when not shut down");

        TTPSegment rcvseg = TTPSegment.extract(rcvpkt);
        if (!rcvseg.isACK())
            return;

        if (state == State.WAIT_FOR_SYNACK && rcvseg.isSYN() && rcvseg.isACK()) {

            unackedQ.poll();
            dispatcherLoop.stopTimer();
            dispatcherLoop.setConnectionState(TTPSocket.ConnectionState.CONNECTED);
            synchronized (dispatcherLoop) {
                dispatcherLoop.notifyAll();
            }
            state = State.WAIT;

        } else if (state == State.WAIT) {

            processACK(rcvseg);

        } else if (state == State.WAIT_FOR_ACK) {

            if (rcvseg.getAcknum() == finseqnum) {
                processACK(rcvseg);
                if (clientReceiver.receivedFIN())
                    state = State.SHUTDOWN;
                else
                    state = State.WAIT_FOR_FIN;
            } else  {
                processACK(rcvseg);
            }

        }
    }

    public void timeout() throws IOException {
        if (state == State.NOT_INITIALISED || state == State.SHUTDOWN)
            throw new IllegalStateException("TTPClientSender::timeout occured when the sender was unhandshaken or shutdown");
        dispatcherLoop.startTimer(estimatedTimeout);

        if (state == State.WAIT_FOR_SYNACK) {

            out.send(unackedQ.peek());

        } else if (state == State.WAIT || state == State.WAIT_FOR_ACK) {

            if (!unackedQ.isEmpty()) {

                for (DatagramPacket pkt: unackedQ)
                    out.send(pkt);

            } else if (!tosendQ.isEmpty()) {

                while (!tosendQ.isEmpty() && unackedQ.size() <= WINDOW_SIZE) {
                    DatagramPacket sndpkt = tosendQ.poll();
                    out.send(sndpkt);
                    unackedQ.offer(sndpkt);
                }

            } else {

                throw new IllegalStateException();

            }
        }
    }

    public void notifyFINReceived() {
        if (state == State.WAIT_FOR_FIN)
            state = State.SHUTDOWN;
        else if (state != State.WAIT_FOR_ACK)
            throw new IllegalStateException();
    }

    @Override
    public void close() throws IOException {
        if (state != State.WAIT)
            throw new IllegalStateException(String.format("TTPClientSender::close called when TTPClientSender.state == %s", state));

        DatagramPacket finpkt = new TTPSegment(nextseqnum, 0, TTPSegment.FIN, null)
                .encapsulate(targetAddress, targetPort);
        send(finpkt);
        finseqnum = nextseqnum - 1;
        state = State.WAIT_FOR_ACK;
        sentFIN = true;
        if (clientReceiver.receivedFIN())
            clientReceiver.notifyFINSent();
    }

    private void processACK(TTPSegment rcvseg) {
        int newbase = rcvseg.getAcknum() + 1;
        int n_acked = newbase - base;
        while (n_acked-- != 0)
            unackedQ.poll();
        base = newbase;
        if (base == nextseqnum)
            dispatcherLoop.stopTimer();
        else
            dispatcherLoop.startTimer(estimatedTimeout);
    }

    private void send(DatagramPacket sndpkt) throws IOException {
        if (nextseqnum < base + WINDOW_SIZE) {
            out.send(sndpkt);
            unackedQ.offer(sndpkt);
            if (nextseqnum == base)
                dispatcherLoop.startTimer(estimatedTimeout);
        } else {
            tosendQ.offer(sndpkt);
        }
        nextseqnum++;
    }

    public boolean hasUnackedSegs() {
        return !unackedQ.isEmpty();
    }

    public boolean sentFIN() {
        return sentFIN;
    }

    public boolean isShut() {
        return state == State.SHUTDOWN;
    }
}

package ttp;

import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Abstraction implementing a ServerSocket, which allocates connection sockets
 * upon receiving a SYN segment.
 */
public class TTPServerSocket implements AutoCloseable {

    private enum State {
        WAIT,
        LISTEN_FOR_SYN,
        WAIT_FOR_COMPLETION
    }

    private State state;
    private final DatagramSocket serverSocket;
    private DatagramPacket synack;

    private TTPSocket connectionSocket;
    private final AtomicBoolean handshakeComplete = new AtomicBoolean(false);

    public TTPServerSocket(int srcPort) throws SocketException {
        this.serverSocket = new DatagramSocket(srcPort);
        serverSocket.setSoTimeout(100);  // ms
        this.state = State.WAIT;
    }

    /**
     * Blocks till a connection is accepted. Returns a connection TTPSocket.
     * @return connection TTPSocket
     */
    public TTPSocket accept() throws IOException {
        this.state = State.LISTEN_FOR_SYN;

        byte[] buf = new byte[1024];
        DatagramPacket rcvpkt = new DatagramPacket(buf, buf.length);

        while (true) {

            if (state == State.LISTEN_FOR_SYN || state == State.WAIT_FOR_COMPLETION) {
                try {
                    serverSocket.receive(rcvpkt);
                    callFromBelow(rcvpkt);
                } catch (SocketTimeoutException x) {
                }
            }

            if (handshakeComplete.get()) {
                serverSocket.disconnect();
                handshakeComplete.set(false);
                state = State.WAIT;
                return connectionSocket;
            }
        }
    }

    public void callFromBelow(DatagramPacket rcvpkt) throws IOException {
        if (state == State.WAIT)
            throw new IllegalStateException();

        TTPSegment rcvseg = TTPSegment.extract(rcvpkt);

        if (rcvseg.isSYN()) {
            if (state == State.LISTEN_FOR_SYN) {

                serverSocket.connect(rcvpkt.getAddress(), rcvpkt.getPort());

                int initialseqnum = TTPSocket.makeInitialSeqnum();
                int expectedseqnum = rcvseg.getSeqnum() + 1;
                InetAddress targetAddress = rcvpkt.getAddress();
                int targetPort = rcvpkt.getPort();
                connectionSocket = new TTPSocket(this, targetAddress, targetPort, initialseqnum + 1, expectedseqnum);

                byte flags = TTPSegment.SYN | TTPSegment.ACK;
                byte[] data = ByteBuffer.allocate(4)
                             .putInt(connectionSocket.getLocalPort()).array();
                synack = new TTPSegment(initialseqnum, rcvseg.getSeqnum(), flags, data)
                            .encapsulate(rcvpkt.getAddress(), rcvpkt.getPort());
                serverSocket.send(synack);

                state = State.WAIT_FOR_COMPLETION;

            } else if (state == State.WAIT_FOR_COMPLETION) {

                serverSocket.send(synack);

            }
        }
    }

    /**
     * Called by the TTPSocket dispatcher to notify that a connection was
     * successfully completed
     */
    public void notifyHandshakeComplete() {
        handshakeComplete.set(true);
    }
    

    @Override
    public void close() {
        serverSocket.close();
    }
}
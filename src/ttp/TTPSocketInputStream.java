package ttp;

import java.io.InputStream;

/**
 * Class implementing a lossless stream abstraction on the message transport
 * mechanism provided by TTPSocket
 */
public class TTPSocketInputStream extends InputStream {

    private final TTPSocket socket;
    private byte[] currentMessage;
    private int currentMessageIdx;
    private boolean eof = false;

    public TTPSocketInputStream(TTPSocket socket) {
        this.socket = socket;
        this.currentMessageIdx = 0;
    }

    @Override
    public int available() {
        return currentMessage.length - currentMessageIdx - 1;
    }

    @Override
    public int read() {
        if (currentMessage == null || currentMessageIdx == currentMessage.length) {
            if (socket.isMessageAvailable())
                loadNextMessage();
            else if (socket.isReceiverShut())
                return -1;
        }
        return currentMessage[currentMessageIdx++];
    }

    @Override
    public int read(byte[] b) {
        return read(b, 0, b.length);
    }

    @Override
    public int read(byte[] b, int off, int len) {
        int to_read = len, read = 0;
        while (true) {
            if (currentMessage == null || currentMessageIdx == currentMessage.length) {
                if (!socket.isReceiverShut())
                    // If we have no message ready, load the next one, waiting for it if not immediately available.
                    loadNextMessage();
                else if (socket.isMessageAvailable())
                    loadNextMessage();
                else
                    // If we have no message ready, and the connection has been closed, close the stream.
                    return -1;
            }
            int copy_from_this_msg = Math.min(currentMessage.length, to_read);
            System.arraycopy(currentMessage, currentMessageIdx, b, off, copy_from_this_msg);
            off += copy_from_this_msg;
            currentMessageIdx += copy_from_this_msg;
            read += copy_from_this_msg;
            to_read -= copy_from_this_msg;
            if (read == len)
                return read;
            else if (!socket.isMessageAvailable()) // If no message is immediately available, return.
                return read;
        }
    }

    /**
     * Returns only if a message is available, waits until one is available otherwise.
     */
    public void loadNextMessage() {
        currentMessage = socket.getLatestMessage();
        currentMessageIdx = 0;
    }
}

package ttp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.ByteBuffer;

/**
 * Implements a client side TTP Receiver.
 */
public class TTPClientReceiver {

    /**
     * Returns true if the receiver is or will be in a state which allows for receiving messages.
     * @return true if the state of the receiver is WAIT or WAIT_FOR_SYNACK, false otherwise.
     */
    public boolean isWaiting() {
        return state == State.WAIT ||
                state == State.WAIT_FOR_SYNACK;
    }

    protected enum State {
        WAIT_FOR_SYNACK,
        WAIT,
        WAIT_FOR_CLOSE,
        WAITING_FOR_TIMEOUT,
        SHUTDOWN
    }

    private int expectedseqnum;
    private final InetAddress target_address;
    private int target_port;
    private final DatagramSocket out;
    private DatagramPacket sndpkt;
    private final TTPSocket dispatcherLoop;
    private TTPClientSender clientSender;

    protected State state = State.WAIT_FOR_SYNACK;
    private boolean receivedFIN;

    public TTPClientReceiver(TTPSocket dispatcherLoop, DatagramSocket out,
                             InetAddress target_address, int target_port) {
        this.dispatcherLoop = dispatcherLoop;
        this.out = out;
        this.target_address = target_address;
        this.target_port = target_port;
    }

    /**
     * Processes packets inbound from the network
     * @param rcvpkt packet received from the network
     * @throws IOException if sending a reply segment fails
     */
    public void callFromBelow(DatagramPacket rcvpkt) throws IOException {
        if (state == State.SHUTDOWN)
            throw new IllegalStateException("Data passed to a shut down receiver");

        TTPSegment rcvseg = TTPSegment.extract(rcvpkt);
        if (!rcvseg.isFIN() && !rcvseg.hasData())
            return;

        if (state == State.WAIT_FOR_SYNACK && rcvseg.isSYN() && rcvseg.isACK()) {

            expectedseqnum = rcvseg.getSeqnum();
            target_port = ByteBuffer.wrap(rcvseg.getData()).getInt();
            clientSender.setTargetPort(target_port);
            sndpkt = new TTPSegment(0, expectedseqnum, TTPSegment.ACK, null)
                    .encapsulate(target_address, target_port);
            expectedseqnum++;
            out.disconnect();
            out.connect(target_address, target_port);
            out.send(sndpkt);
            dispatcherLoop.setConnectionState(TTPSocket.ConnectionState.CONNECTED);
            state = State.WAIT;

        } else if (state == State.WAIT) {

            if (rcvseg.getSeqnum() != expectedseqnum) {
                out.send(sndpkt);
            } else if (!rcvseg.isFIN()) {
                dispatcherLoop.deliverData(rcvseg.getData());
                sndpkt = new TTPSegment(0, expectedseqnum, TTPSegment.ACK, null)
                        .encapsulate(target_address, target_port);
                out.send(sndpkt);
                expectedseqnum++;
            } else if (rcvseg.isFIN()) {
                if (clientSender.sentFIN()) {
                    sndpkt = new TTPSegment(0, expectedseqnum, TTPSegment.ACK, null)
                            .encapsulate(target_address, target_port);
                    out.send(sndpkt);
                    expectedseqnum++;
                    dispatcherLoop.startTeardownTimer();
                    receivedFIN = true;
                    clientSender.notifyFINReceived();
                    state = State.WAITING_FOR_TIMEOUT;
                } else {
                    sndpkt = new TTPSegment(0, expectedseqnum, TTPSegment.ACK, null)
                            .encapsulate(target_address, target_port);
                    out.send(sndpkt);
                    expectedseqnum++;
                    receivedFIN = true;
                    state = State.WAIT_FOR_CLOSE;
                }
            }

        } else if (state == State.WAITING_FOR_TIMEOUT
                || state == State.WAIT_FOR_CLOSE) {

            out.send(sndpkt);

        }
    }

    /**
     * Used by the Sender to notify the receiver that it can shut down.
     */
    public void notifyFINSent() {
        if (state != State.WAIT_FOR_CLOSE)
            throw new IllegalStateException();
        state = State.SHUTDOWN;
    }

    /**
     * Used by the Sender to check if a FIN was received
     * @return true if a FIN segment was received
     */
    public boolean receivedFIN() {
        return receivedFIN;
    }

    /**
     * Called after the teardown timer terminates
     */
    public void timeout() {
        if (state != State.WAITING_FOR_TIMEOUT)
            throw new IllegalStateException("Timeout occured while a receiver wasn't closing");
        state = State.SHUTDOWN;
    }

    /**
     * Since the sender needs a receiver and vice versa, mutual references cant be passed in constructors.
     * @param clientSender
     */
    protected void setSender(TTPClientSender clientSender) {
        this.clientSender = clientSender;
    }

    public boolean isShut() {
        return state == State.SHUTDOWN;
    }
}

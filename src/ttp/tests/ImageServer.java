package ttp.tests;

import ttp.TTPServerSocket;
import ttp.TTPSocket;

import java.io.FileInputStream;
import java.io.OutputStream;

public class ImageServer {
    public static void main(String[] args) {
        while (true) {
            try (
                    TTPServerSocket ss = new TTPServerSocket(44004);
                    TTPSocket s = ss.accept();
                    OutputStream out = s.getOutputStream();
            ) {
                FileInputStream file = new FileInputStream("image.jpg");
                out.write(file.readAllBytes());
            } catch (Exception x) {
                x.printStackTrace();
            }
        }
    }
}

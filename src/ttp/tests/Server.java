package ttp.tests;

import ttp.TTPServerSocket;
import ttp.TTPSocket;

import java.io.InputStream;
import java.io.OutputStream;

public class Server {
    public static void main(String[] args) {
        try (
                TTPServerSocket ss = new TTPServerSocket(12345);
                TTPSocket s = ss.accept();
                OutputStream out = s.getOutputStream();
                InputStream in = s.getInputStream();
        ) {
            String msg = "byte order, huh?";
            out.write(("Server: " + msg).getBytes());
        } catch (Exception x) {
            System.out.println(x);
        }
    }
}

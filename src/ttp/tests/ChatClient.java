package ttp.tests;


import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

import ttp.TTPSocket;

public class ChatClient {

    private TTPSocket s;

    public ChatClient(TTPSocket s) {
        try (s) {
            Thread writer = new Thread() {
                @Override
                public void run() {
                    try (
                            PrintWriter pw = new PrintWriter(new OutputStreamWriter(s.getOutputStream()));
                            BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in))
                    ) {
                        String line = null;
                        System.out.print("> ");
                        while ((line = stdin.readLine()) != null) {
                            pw.println(line);
                            System.out.print("> ");
                            pw.flush();
                        }
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }
            };
            writer.start();

            try (
                    BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()))
            ) {
                String line = null;
                while ((line = in.readLine()) != null) {
                    System.out.format("\b\b< %s\n> ", line);
                }
            }
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        if (args.length < 2)
            return;

        InetAddress ip = InetAddress.getByName(args[0]);
        int port = Integer.parseInt(args[1]);

        try (TTPSocket s = new TTPSocket(ip, port)) {
            new ChatClient(s);
        }
    }
}

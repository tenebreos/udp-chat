package ttp.tests;

import ttp.TTPSocket;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.*;
import java.net.InetAddress;
import java.net.URL;
import javax.swing.*;

/**
 * A program for viewing images.
 * @version 1.30 2014-02-27
 * @author Cay Horstmann
 */
public class ImageClient
{
    public static void main(String[] args) throws Exception
    {
        if (args.length < 2) {
            System.out.println("Usage: ImageClient <hostname> port");
        }

        InetAddress addr;
        try {
            addr = InetAddress.getByName(args[0]);
        } catch (Exception x) {
            System.out.println("Host not found.");
            return;
        }

        int port = Integer.parseInt(args[1]);

        EventQueue.invokeLater(() -> {
            JFrame frame = new ImageViewerFrame(addr, port);
            frame.setTitle("ImageViewer");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
        });
    }
}

/**
 * A frame with a label to show an image.
 */
class ImageViewerFrame extends JFrame
{
    private final JLabel label;
    private final JFileChooser chooser;
    private static final int DEFAULT_WIDTH = 300;
    private static final int DEFAULT_HEIGHT = 400;

    public ImageViewerFrame(InetAddress addr, int port)
    {
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        setLayout(new BorderLayout());

        // use a scroll pane to allow larger images on display
        JScrollPane imageScrollPane = new JScrollPane();
        add(imageScrollPane, BorderLayout.CENTER);

        // use a label to display the images
        label = new JLabel();
        imageScrollPane.setViewportView(label);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setVerticalAlignment(SwingConstants.CENTER);

        // set up the file chooser
        chooser = new JFileChooser();
        chooser.setCurrentDirectory(new File("."));

        // set up the menu bar
        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);

        JMenu menu = new JMenu("File");
        menuBar.add(menu);

        JMenuItem openItem = new JMenuItem("Open");
        menu.add(openItem);
        openItem.addActionListener(event -> {
            // show file chooser dialog
            int result = chooser.showOpenDialog(null);

            // if file selected, set it as icon of the label
            if (result == JFileChooser.APPROVE_OPTION)
            {
                String name = chooser.getSelectedFile().getPath();
                label.setIcon(new ImageIcon(name));
            }
        });

        JMenuItem loadItem = new JMenuItem("Load from server");
        menu.add(loadItem);
        loadItem.addActionListener(event -> {
            try {
                label.setIcon(new ImageIcon(loadImage(addr, port)));
            } catch (Exception exception) {
                System.err.println("Error loading image.");
                System.err.println(exception.getMessage());
            }
        });

        JMenuItem exitItem = new JMenuItem("Exit");
        menu.add(exitItem);
        exitItem.addActionListener(event -> System.exit(0));
    }

    private byte[] loadImage(InetAddress addr, int port) {
        try (
                TTPSocket s = new TTPSocket(addr, port);
                InputStream in = s.getInputStream();
                FileOutputStream outfile = new FileOutputStream("received.jpg");
        ) {
            return in.readAllBytes();
        } catch (Exception x) {
            return null;
        }
    }
}

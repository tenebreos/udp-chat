package ttp.tests;

import ttp.TTPSocket;

import java.io.InputStream;
import java.io.OutputStream;

public class Client {
    public static void main(String[] args) {
        try (
                TTPSocket s = new TTPSocket("localhost", 12345);
                InputStream in = s.getInputStream();
                OutputStream out = s.getOutputStream()
        ) {
            System.out.println(new String(in.readAllBytes()));
        } catch (Exception x) {
            x.printStackTrace();
        }
    }
}

package ttp.tests;

import java.net.*;

import ttp.TTPServerSocket;
import ttp.TTPSocket;

import java.io.*;
import java.net.InetAddress;

public class ChatServer {
    public static void main(String[] args) throws Exception {
        if (args.length < 1)
            return;

        int port = Integer.parseInt(args[0]);

        try (
                TTPServerSocket ss = new TTPServerSocket(port);
                TTPSocket s = ss.accept();
        ) {
            new ChatClient(s);
        }
    }
}

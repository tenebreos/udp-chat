package ttp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class TTPServerReceiver {

    public boolean isWaiting() {
        return state == State.WAIT ||
                state == State.NOT_INITIALISED;
    }

    protected enum State {
        NOT_INITIALISED,
        WAIT,
        WAIT_FOR_CLOSE,
        WAITING_FOR_TIMEOUT,
        SHUTDOWN
    }

    private int expectedseqnum = 0;
    private InetAddress target_address;
    private int target_port;
    private DatagramSocket out;
    private DatagramPacket sndpkt;
    private TTPSocket dispatcherLoop;
    private TTPServerSender serverSender;

    protected State state = State.NOT_INITIALISED;
    private boolean receivedFIN;

    public TTPServerReceiver(TTPSocket dispatcherLoop, DatagramSocket out,
                             InetAddress target_address, int target_port,
                             int expectedseqnum) {
        this.out = out;
        this.expectedseqnum = expectedseqnum;
        this.target_address = target_address;
        this.target_port = target_port;
        this.dispatcherLoop = dispatcherLoop;
    }

    public void callFromBelow(DatagramPacket rcvpkt) throws IOException {
        if (state == State.SHUTDOWN || state == State.NOT_INITIALISED)
            throw new IllegalStateException("receiver may not accept packets while unhandshaken or shut down");

        TTPSegment rcvseg = TTPSegment.extract(rcvpkt);
        if (!rcvseg.isFIN() && !rcvseg.hasData())
            return;

        if (state == State.WAIT) {

            if (rcvseg.getSeqnum() != expectedseqnum) {
                out.send(sndpkt);
            } else if (!rcvseg.isFIN()) {
                dispatcherLoop.deliverData(rcvseg.getData());
                sndpkt = new TTPSegment(0, expectedseqnum, TTPSegment.ACK, null)
                        .encapsulate(target_address, target_port);
                out.send(sndpkt);
                expectedseqnum++;
            } else if (rcvseg.isFIN()) {
                if (serverSender.sentFIN()) {
                    sndpkt = new TTPSegment(0, expectedseqnum, TTPSegment.ACK, null)
                            .encapsulate(target_address, target_port);
                    out.send(sndpkt);
                    expectedseqnum++;
                    dispatcherLoop.startTeardownTimer();
                    receivedFIN = true;
                    serverSender.notifyFINReceived();
                    state = State.WAITING_FOR_TIMEOUT;
                } else {
                    sndpkt = new TTPSegment(0, expectedseqnum, TTPSegment.ACK, null)
                            .encapsulate(target_address, target_port);
                    out.send(sndpkt);
                    expectedseqnum++;
                    receivedFIN = true;
                    state = State.WAIT_FOR_CLOSE;
                }
            }

        } else if (state == State.WAITING_FOR_TIMEOUT
                || state == State.WAIT_FOR_CLOSE) {

            out.send(sndpkt);

        }
    }

    public void notifyFINSent() {
        if (state != State.WAIT_FOR_CLOSE)
            throw new IllegalStateException();
        state = State.SHUTDOWN;
    }

    public boolean receivedFIN() {
        return receivedFIN;
    }

    public void timeout() {
        if (state != State.WAITING_FOR_TIMEOUT)
            throw new IllegalStateException("Timeout should only occur while waiting for a shutdown");
        state = State.SHUTDOWN;
        dispatcherLoop.setConnectionState(TTPSocket.ConnectionState.SHUTDOWN);
    }

    public void setSender(TTPServerSender serverSender) {
        this.serverSender = serverSender;
    }

    public boolean isShut() {
        return state == State.SHUTDOWN;
    }

    public void notifyHandshakeComplete() {
        if (state != State.NOT_INITIALISED)
            throw new IllegalStateException("notifyHandshake called on an already handshaken socket");
        state = State.WAIT;
    }
}
